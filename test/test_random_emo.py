import threading
import unittest

from src.optim.random_emo import EMOServer, EMORequestHandler
from test.test_base_optim import BaseOptimServerTests


class EMOServerTests(BaseOptimServerTests):
    def setUp(self):
        # Start an EMOServer
        self.emo_server = EMOServer(('localhost', 8001), EMORequestHandler)
        emo_thread = threading.Thread(target=self.emo_server.serve_forever)
        emo_thread.start()

    def tearDown(self):
        self.emo_server.shutdown()
        self.emo_server.socket.close()

    def test_optimize_empty(self):
        self._test_optimize_empty()

    def test_optimize_1region_1measure_1week(self):
        self._test_optimize_1region_1measure_1week()

    def test_optimize_13regions_1measure_7weeks(self):
        self._test_optimize_13regions_1measure_7weeks()

    def test_optimize_13regions_4measures_7weeks(self):
        self._test_optimize_13regions_4measures_7weeks()


if __name__ == '__main__':
    unittest.main()
