import threading
import unittest

from src.model.macromodel import ModelServer, ModelRequestHandler
from src.optim.base_optim import BaseOptimServer
from src.optim.branch_and_bound_optim import BBOptimRequestHandler
from test.test_base_optim import BaseOptimServerTests


class BBOptimServerTests(BaseOptimServerTests):
    def setUp(self):
        # Start an OptimServer and a ModelServer
        self.model_server = ModelServer(('localhost', 8000), ModelRequestHandler)
        self.optim_server = BaseOptimServer(('localhost', 8001), BBOptimRequestHandler, draw=False)
        model_thread = threading.Thread(target=self.model_server.serve_forever)
        model_thread.start()
        optim_thread = threading.Thread(target=self.optim_server.serve_forever)
        optim_thread.start()

    def tearDown(self):
        self.model_server.shutdown()
        self.model_server.socket.close()
        self.optim_server.shutdown()
        self.optim_server.socket.close()

    def test_optimize_empty(self):
        self._test_optimize_empty()

    def test_optimize_1region_1measure_1week(self):
        self._test_optimize_1region_1measure_1week()

    def test_optimize_13regions_1measure_7weeks(self):
        self._test_optimize_13regions_1measure_7weeks()

    def test_optimize_13regions_4measures_7weeks(self):
        self._test_optimize_13regions_4measures_7weeks()


if __name__ == '__main__':
    unittest.main()
