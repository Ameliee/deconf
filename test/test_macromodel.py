import json
import threading
import unittest

import numpy as np
import pandas as pd
import requests
from numpy.testing import assert_almost_equal

from src.model.economie import path_to_root, Economie
from src.model.macromodel import ModelServer, ModelRequestHandler
from src.model.model_seair import default_parameters, SEAIRModel, SEAIR2params, SEAIR_ratios2params


class ModelTest(unittest.TestCase):
    def assertNPAlmostEqual(self, first, second, decimal=7):
        assert_almost_equal(first, second, decimal)

    def test_economie(self):
        filename = path_to_root + "/doc/eval_policy_req.json"
        eco = Economie()
        eco_results = eco.evaluate(filename, 10)
        self.assertEqual(1 + 10, len(eco_results))  # includes week 0
        self.assertEqual(0, eco_results[0])  # week 0
        self.assertTrue(-5 <= eco_results[-1] <= 0)

    def test_simple(self):
        params = default_parameters()
        self.assertEqual(1, params['N0'])
        self.assertEqual(0, params['I0'])
        self.assertEqual(2.5, params['R0'])
        self.assertListEqual([0, 365 * 2], params['time_interval'])

        model = SEAIRModel(params)
        results = model.compute()
        self.assertEqual((1 + 365 * 2, 10), results.shape)
        S0 = 1 - 0
        self.assertNPAlmostEqual([1, 0, 0, 0, 0, 0, 0, 0, 0, 0], results[0])
        # print(results[0][:])
        # results[-1] = Alizon values before optimizing
        # print(results[-1][:])
        self.assertNPAlmostEqual(
            [1.07102563e-01, 1.23173643e-06, 3.08034100e-06, 2.06595723e-05, 8.04239721e-01, 1.36859604e-07,
             3.42260112e-07, 2.34286944e-06, 7.59559334e-02, 1.34039883e-02], results[-1])
        # results.to_csv("output.json")

    def test_big_pop(self):
        params = default_parameters()
        params['N0'] = 1000 * 1000
        params['I0'] = 200 * 1000
        model = SEAIRModel(params)
        results = model.compute()
        ratio_I1 = params['y0'][3]
        self.assertEqual(0.8, ratio_I1)
        self.assertEqual((1 + 365 * 2, 10), results.shape)
        S = results[0][0]
        I1 = results[0][3]
        I2 = results[0][7]
        self.assertAlmostEqual(I1, ratio_I1 * 200 * 1000)
        self.assertAlmostEqual(I2, (1 - ratio_I1) * 200 * 1000)
        self.assertAlmostEqual(S, 800 * 1000)
        # print(results[0][:])
        # print(results[-1][:])

    def test_three_periods(self):
        params = default_parameters()
        model = SEAIRModel(params)
        results = model.compute(time_interval=[0, 3, 365 * 2], c=[.1, .2, 0])
        self.assertEqual((1 + 365 * 2, 10), results.shape)

    def test_SEAIR2params(self):
        params = default_parameters()
        SEAIR2params(S=1, E1=2, E2=3, A1=4, A2=5, I1=6, I2=7, R1=8, R2=9, M=10, params=params)
        self.assertEqual(10 * 11 / 2, params['N0'])
        self.assertEqual(1, params['y0'][0])  # S
        self.assertEqual(6 / 13, params['y0'][3])  # mild
        self.assertEqual(7 / 13, params['y0'][7])  # not mild

    def test_SEAIR_ratios2params(self):
        params = default_parameters()
        SEAIR_ratios2params(npop=100, S=.45, E1=.05, E2=.05, A1=.05, A2=.05, I1=.05, I2=.05, R1=.1, R2=.1, M=.05,
                            params=params)
        self.assertAlmostEqual(100, params['N0'])
        self.assertAlmostEqual(45, params['y0'][0])  # S
        self.assertAlmostEqual(5, params['y0'][1])  # E1
        self.assertAlmostEqual(5, params['y0'][2])  # A1
        self.assertAlmostEqual(.5, params['y0'][3])  # mild
        self.assertAlmostEqual(10, params['y0'][4])  # R1
        self.assertAlmostEqual(5, params['y0'][5])  # E2
        self.assertAlmostEqual(5, params['y0'][6])  # A2
        self.assertAlmostEqual(.5, params['y0'][7])  # not mild
        self.assertAlmostEqual(10, params['y0'][8])  # R2
        self.assertAlmostEqual(5, params['y0'][9])  # M

    def test_SEAIRModel_nocontrol(self):
        params = default_parameters()
        model = SEAIRModel(params)
        nocontrol = model.compute()
        alizon_nocontrol = pd.read_csv("./nocontrol_alizon.csv", index_col=0)
        names = zip(list(alizon_nocontrol.columns[1: -1]), list(np.arange(10)))
        for idx_ali, idx_py in names:
            self.assertTrue(np.allclose(alizon_nocontrol[idx_ali].values, nocontrol[:, idx_py], atol=1e-2))

    def test_SEAIRModel_strongcontrol(self):
        params = default_parameters()
        params["c"] = [0, 0.9, 0]
        params["time_interval"] = [0, 60, 90, 730]
        model = SEAIRModel(params)
        strong_control = model.compute()
        alizon_strongcontrol = pd.read_csv("./strongcontrol_alizon.csv", index_col=0)
        names = zip(list(alizon_strongcontrol.columns[1: -1]), list(np.arange(10)))
        for idx_ali, idx_py in names:
            self.assertTrue(np.allclose(alizon_strongcontrol[idx_ali].values, strong_control[:, idx_py], atol=1e-2))

    def test_SEAIRModel_longcontrol(self):
        params = default_parameters()
        params["c"] = [0, 0.4, 0]
        params["time_interval"] = [0, 60, 425, 730]
        model = SEAIRModel(params)
        long_control = model.compute()
        alizon_longcontrol = pd.read_csv("./longcontrol_alizon.csv", index_col=0)
        names = zip(list(alizon_longcontrol.columns[1: -1]), list(np.arange(10)))
        for idx_ali, idx_py in names:
            self.assertTrue(np.allclose(alizon_longcontrol[idx_ali].values, long_control[:, idx_py], atol=1e-2))


class ServerTests(unittest.TestCase):

    # https://github.com/slok/prometheus-python/blob/master/prometheus/test_pusher.py
    def setUp(self):
        # print('setUp')
        self.server = ModelServer(('localhost', 8000), ModelRequestHandler)
        # Start a server
        thread = threading.Thread(target=self.server.serve_forever)
        thread.start()

    def tearDown(self):
        # print('tearDown')
        self.server.shutdown()
        self.server.socket.close()

    def test_eval_policy_empty(self):
        URL = 'http://localhost:8000/eval_policy'
        with open('../doc/empty.json', 'rb') as data:
            response = requests.post(URL, data=data)
            self.assertTrue(response.ok)
            resp = response.json()
            self.assertFalse(resp['success'])

    def test_eval_policy_one_zone(self):
        pass
        URL = 'http://localhost:8000/eval_policy'
        with open('../doc/eval_policy_req.json', 'rb') as json_data:
            data = json.load(json_data)
            data['zones'] = ['FR-BFC']
            data['population_size'] = [2795301]
            response = requests.post(URL, data=json.dumps(data))
            self.assertTrue(response.ok)
            resp = response.json()
            self.assertTrue(resp['success'])
            nweeks, nzones = 2 * 52 + 1, 1
            self.assertEqual(nweeks, len(resp['gdp_accumulated_loss']))
            self.assertEqual(nweeks, len(resp['cases']))
            self.assertEqual(nzones, len(resp['cases'][0]['initial']))
            self.assertEqual(nzones, len(resp['cases'][-1]['week']))

    def test_eval_policy_ok(self):
        URL = 'http://localhost:8000/eval_policy'
        with open('../doc/eval_policy_req.json', 'rb') as data:
            response = requests.post(URL, data=data)
            self.assertTrue(response.ok)
            resp = response.json()
            self.assertTrue(resp['success'])
            self.assertListEqual(resp['zones'],
                                 ["FR-ARA", "FR-BFC", "FR-BRE", "FR-CVL", "FR-COR", "FR-GES", "FR-HDF", "FR-IDF",
                                  "FR-NOR", "FR-NAQ", "FR-OCC", "FR-PDL", "FR-PAC"])
            self.assertEqual('Alizon20', resp['compartmental_model'])
            self.assertEqual(731, resp['max_days'])
            nweeks, nzones = 2 * 52 + 1, 13
            # TODO tests on gdp_accumulated_loss
            self.assertEqual(nweeks, len(resp['gdp_accumulated_loss']))
            self.assertEqual(nweeks, len(resp['cases']))
            self.assertEqual(nzones, len(resp['cases'][0]['initial']))
            self.assertEqual(nzones, len(resp['cases'][-1]['week']))
            self.assertEqual(nweeks, len(resp['hospital_beds']))
            self.assertEqual(nweeks, len(resp['ICU_beds']))
            self.assertEqual(nweeks, len(resp['deaths']))
            self.assertEqual(nweeks, len(resp['new_cases']))
            # check for week 0 (initial conditions) of FR-BFC (model #1)
            I2begin = resp['hospital_beds'][0]['initial'][1]
            I1begin = resp['cases'][0]['initial'][1] - I2begin
            self.assertAlmostEqual(2795301 * 0.0006900549635215819, I2begin)
            self.assertAlmostEqual(2795301 * 0.006157002093575612, I1begin)
            I2end = resp['hospital_beds'][-1]['week'][1]
            I1end = resp['cases'][-1]['week'][1] - I2end
            Mend = resp['deaths'][-1]['week'][1]
            self.assertLess(I1end, 1)  # nobody sick anymore
            self.assertLess(I2end, 1)  # nobody sick anymore
            self.assertGreater(Mend, 0)  # 330664.9625104317


if __name__ == '__main__':
    unittest.main()
