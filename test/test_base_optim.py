import json
import threading
import unittest

import matplotlib
import requests

from src.model.macromodel import ModelServer, ModelRequestHandler
from src.optim.base_optim import BaseOptimServer, BaseOptimRequestHandler


class BaseOptimServerTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _test_optimize_empty(self):
        URL = 'http://localhost:8001/optimize'
        with open('../doc/empty.json', 'rb') as data:
            response = requests.post(URL, data=data)
            self.assertTrue(response.ok)
            resp = response.json()
            self.assertFalse(resp['success'])

    def _test_optimize_1region_1measure_1week(self):
        URL = 'http://localhost:8001/optimize'
        with open('../doc/optimize_req.json', 'rb') as json_data:
            data = json.load(json_data)
            data['zones'] = ['FR-BFC']
            data['population_size'] = [2795301]
            data['max_measure_weeks'] = 1
            response = requests.post(URL, data=json.dumps(data))
            self.assertTrue(response.ok)
            resp = response.json()
            # check results
            self.assertTrue(resp['success'])
            self.assertIsNone(resp['social_containment_all'])
            self.assertIsNone(resp['total_containment_high_risk'])
            self.assertIsNone(resp['forced_telecommuting'])
            self.assertEqual([0, 1], resp['measure_weeks'])
            self.assertEqual(2, len(resp['school_closures']))
            self.assertEqual([False], resp['school_closures'][-1]['activated'])  # all schools open at end

    def _test_optimize_13regions_1measure_7weeks(self):
        URL = 'http://localhost:8001/optimize'
        with open('../doc/optimize_req.json', 'rb') as data:
            response = requests.post(URL, data=data)
            self.assertTrue(response.ok)
            resp = response.json()
            # check results
            self.assertTrue(resp['success'])
            self.assertIsNone(resp['social_containment_all'])
            self.assertIsNone(resp['total_containment_high_risk'])
            self.assertIsNone(resp['forced_telecommuting'])
            self.assertTrue(2 <= len(resp['measure_weeks']) <= 7 + 1)
            self.assertEqual(len(resp['measure_weeks']), len(resp['school_closures']))
            self.assertEqual([False] * 13, resp['school_closures'][-1]['activated'])  # all schools open at end

    def _test_optimize_13regions_4measures_7weeks(self):
        URL = 'http://localhost:8001/optimize'
        with open('../doc/optimize_req.json', 'rb') as json_data:
            data = json.load(json_data)
            data['activable_measures']['forced_telecommuting'] = True
            data['activable_measures']['social_containment_all'] = True
            data['activable_measures']['total_containment_high_risk'] = True
            response = requests.post(URL, data=json.dumps(data))
            self.assertTrue(response.ok)
            resp = response.json()
            # check results
            self.assertTrue(resp['success'])
            nweeks = len(resp['measure_weeks'])
            self.assertTrue(2 <= nweeks <= 7 + 1)
            self.assertEqual(nweeks, len(resp['forced_telecommuting']))
            self.assertEqual([False] * 13, resp['forced_telecommuting'][-1]['activated'])  # open at end
            self.assertEqual(nweeks, len(resp['school_closures']))
            self.assertEqual([False] * 13, resp['school_closures'][-1]['activated'])  # open at end
            self.assertEqual(nweeks, len(resp['social_containment_all']))
            self.assertEqual([False] * 13, resp['social_containment_all'][-1]['activated'])  # open at end
            self.assertEqual(nweeks, len(resp['total_containment_high_risk']))
            self.assertEqual([False] * 13, resp['total_containment_high_risk'][-1]['activated'])  # open at end
