
Install
=======

Deps:

```bash
$ sudo apt install python-matplotlib  # python-qt5
```

C++:

```bash
$ cd <project_root>
$ mkdir build ; cd build
  Option 1, use default compiling options (no assert, no trace):
  $ cmake ..
  Option 2, configure compiling options with CCMake (press c, toggle options with Return, press c, g):
  $ ccmake ..
$ make
```

Python:

```bash
$ cd <project_root>
$ virtualenv -p python3 venv/
$ source venv/bin/activate
$ pip install -r requirements.txt
```

Unit tests
----------

```bash
$ cd test
$ PYTHONPATH+=.. python -m unittest  --verbose
```

Use
===

You have two alternatives for the model :
  1. run separately macromodel server and optimizer server
  2. run EMO server that embeds both.

Macromodel server (default port 8000)
-----------------------------

Run server:

```bash
$ cd <project_root>
$ source venv/bin/activate
$ PYTHONPATH+=. python src/model/macromodel.py
```
or for custom port configuration:

```bash
$ cd <project_root>
$ bash run/run_model.sh run/config.txt
```

Test with CURL & Post:

```bash
T2$ curl --data @doc/eval_policy_req.json http://localhost:8000/eval_policy > /tmp/eval_policy.json
# use jq to reformat cleanly the file (easier to read)
T2$ jq . /tmp/eval_policy.json | less

{
  "success": true,
  "zones": [ "FR-ARA", "FR-BFC", "FR-BRE", "FR-CVL", "FR-COR", "FR-GES", "FR-HDF", "FR-IDF", "FR-NOR", "FR-NAQ", "FR-OCC", "FR-PDL", "FR-PAC" ],
  "compartmental_model": "Alizon20",
  "max_days": 731,
  "gdp_accumulated_loss": [ 0, -0.5921615345168579, -1.1843230690337159, ... ],
  "deaths": [
    {
      "initial": [ 401334.24999999994, 139765.05, 166469.74999999997, 128337.94999999998, 16958.899999999998, 275909.39999999997, 298913.3, 610668.2, 165953.34999999998, 299350.69999999995, 294640.85, 189327.24999999997, 252973.64999999997 ]
    },
    {
      "week": [ 500983.3773594805, 174467.95741320428, 207803.29022394642, 160203.5701869241, 21169.703493178135, 344416.2143699229, 373131.8585241355, 762293.8167891285, 207158.6708923881, 373677.8625822755, 367798.5822593865, 236336.18404868638, 315785.64149271563 ]
    },
    ...
  ],
  "cases": [ ... ]
  "hospital_beds": [ ... ]
  "ICU_beds": [ ... ]
  "new_cases": [ ... ]
}
```

Optimizer server (default port 8001)
----------------------------

We also run macromodel server in the following snippet :

```bash
$ cd <project_root>
$ source venv/bin/activate
T1$ PYTHONPATH+=. python src/model/macromodel.py
T2$ PYTHONPATH+=. python src/optim/random_optim.py
```

or for custom port configuration:

```bash
$ cd <project_root>
T1$ bash run/run_model.sh run/config.txt
T2$ bash run/run_optim.sh run/config.txt
```

Tests:

```bash
T3$ curl --data @doc/optimize_req.json http://localhost:8001/optimize > /tmp/optimize.json
T3$ jq . /tmp/optimize.json | less
```

EMO server (port 8002)
----------------------

EMO = embedded model optimizer.
This model embeds both a SEAIR multi-macromodel and an optimizer.
As such, we don't have to call lots of POST eval_policy requests,
but use instead the embedded models.

Run the server (embeds macromodel and optim):

```bash
$ cd <project_root>
$ source venv/bin/activate
T1$ PYTHONPATH+=. python src/optim/random_emo.py
```

or for custom port configuration:

```bash
$ cd <project_root>
T1$ bash run/run_embedded_model_optim.sh  run/config.txt
```

Tests:

```bash
T2$ curl --data @doc/optimize_req.json http://localhost:8001/optimize > /tmp/optimize.json
T2$ jq . /tmp/optimize.json | less
```

Frontend
--------

Run:

```bash
$ cd <project_root>
T1$ bash run/run_embedded_model_optim.sh  run/config.txt
T2$ bash run/run_frontend.sh run/config.txt
T3$ firefox  http://localhost:8080/deconf/
```


Contrib
=======


Optimizers
----------

Optimizers are based on two classes:
  - BaseOptimServer: inherits a HTTPServer, will wait for "optim_req" requests,
    when one is received will spawn a BaseOptimRequestHandler
  - BaseOptimRequestHandler: will make the effective optimization

To create a new optimizer, you need to create a class that inherits BaseOptimRequestHandler.
This class must overload
`def optimize(self, optimize_req, policy_common_values, activable_measures_list)`.
Complete doc is in `base_optim.py`.


References
==========

Sujet:
https://docs.google.com/document/d/13XMN5-QulYl_rknikkZebVFJEduW5a8OaOveJ883abY

Backlog:
https://docs.google.com/document/d/1t58MrZ14Zx85fldT-tsAOGAxLozPwDQ3t-wNv_lfjPY

Modèle SEAIR :
http://alizon.ouvaton.org/Rapport3_Modele.html

Codes ISO des régions:
https://fr.wikipedia.org/wiki/R%C3%A9gion_fran%C3%A7aise#Liste_et_caract%C3%A9ristiques_des_r%C3%A9gions_actuelles
