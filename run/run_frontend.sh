#!/bin/sh
#source venv/bin/activate
#pip -q install -r requirements.txt


#Parse config file
{ IFS="," read front_dir front_cmd front_host front_port;
  IFS="," read model_dir model_cmd model_host model_port;
  IFS="," read optim_dir optim_cmd optim_host optim_port optim_draw;
}  < $1

cd src/frontend
#a bunch of data copies to serve files
cp ../../data/regions.geojson  static
cp ../../data/data_emploi_region.json  static
cp ../../data/data_age_region.json  static

cp ../../doc/eval_policy_req.json  static
#cp ../../doc/initial_conditions_20200416.json  static/initial_conditions_req.json
cp ../../doc/optimize_req.json  static

#configure initial conditions
#~ curl -s -X POST -d @static/initial_conditions_req.json ${model_host}:${model_port}/initial_conditions -H "Content-Type: application/json"
#~ echo

curl -s -o static/eval_policy_res.json -X POST -d @static/eval_policy_req.json ${model_host}:${model_port}/eval_policy -H "Content-Type: application/json"

echo

export FLASK_ENV=development
export MODEL_HOST=$model_host
export MODEL_PORT=$model_port
export OPTIM_HOST=$optim_host
export OPTIM_PORT=$optim_port
export FLASK_APP=$front_cmd


flask run --port=$front_port --host=$front_host
