# Notes

Point4:

- Modèle de propagation
- SEIR par Région/Dpt
- Taux de reproduction
- Durée d'incubation
- % porteurs sains


# Analyse de R0
R0:
- 2.5 : [EquipeETE2020a]
- 1.5;3.7 [Abbot2020]

# Modèles complet

## Modèles SEIAR [EquipeETE2020b]

![modele SEIAR](./equipe_ETE_modele_SEAIR.png "Modele SAIR")

Notation | Description | Défaut | Gamme | Référence
----|------------------|--------|-------|----------------
ϵ  | taux de fin de latence                                      | 1/4.2 jours^{-1} | [0.21 ; 0.27]  | [Li2020]
σ  | taux d’apparition des symptomes                             | 1 jour^{-1}      | [0.9 ; 1.1]    | [Ferguson2020a]
γ1 | taux de guérison des infections légères                     | 1/17 jours^{-1}  | [0.025 ; 0.1]  | [Zhou2020]
γ2 | taux de guérison des infections sévères                     | 1/20 jours^{-1}  | [0.039 ; 0.13] | [Zhou2020]
R0R_0| taux de reproduction de base                              |2.5               | [2 ; 3]        | [EquipeETE2020a]
p  | proportion des infections qui ne nécessitent pas l’hospitalisation | 0.9       | [0.85 ; 0.95]  | Santé Publique France
θ  | taux de létalité des infections hospitalisées               | 0.15             | [0.135 ; 0.165]| Santé Publique France
α  | taux de mortalité des cas sévère                            | calculé          | — | formule du taux de létalité
ν  | taux de migration                                           | 10^{-6} jour^{-1}| — | ad hoc
bb | diminution de la transmission via l’hospitalisation         | 0.2              | — | ad hoc
cc | fraction du R0R_0 diminuée par des politiques de santé publique | —            | — | variable

## Modèles SEIR

### Modèle Hellewell2020

![modele Hellewell](./scenario_hellewell2020.png "Modele Hellewell")

Type   | Parameter                     | Description | Measures  | Value            | Reference
-------|-------------------------------|-------|-----------------|------------------|-----
Sample | Delay from onset to isolation | short | Mean (variance) | 3.83 (5.99) days | 22
       |                               | long  | Mean (variance) | 9.1 (19.53) days | 23
       | Incubation Period             |       | Mean (std)      | 5.8 (2.6) days   | 24
       | Serial Interval               |       | Mean (std)      | Incubation period (2) | Assumed
-------|-------------------------------|-------|-----------------|------------------|--
Fixed  | Initial cases                 |       |                 | 5, 20, and 40    | 11, 15 
       | Percentage of contacts traced |       |                 | 0, 20, 40, 60, 80, 100% | Tested
       | RO                            | Lower, Central, higher estimated| | 1.5, 2.5, 3.5 | 18, 19
       | Overdispersion in R0          | SARS-like, Flu-like |   | 0.16 | 20
       | R0 after isolation            |       |                 | 0                | Assumed 
       | Cases isolated once identified|       |                 | 100 %            | Assumed 
       | Isolation effectiveness       |       |                 | 100 %            | Assumed 
       | Subclinical infectionn %      |       |                 | 0, 10            | Tested


### Abbot2020

### Kucharsky 2020

Parameter 	      | Value    |  	Distribution | 	Source
------------------|----------|--------------|-----
Incubation period | 5.2 days | Erlang (shape=2) | Li et al
Infectious period | 2.9 days | Erlang (shape=2) | Liu et al, Xu et al
Delay onset-to-confirmation | 6.1 days | Exponential | Xu et al
Daily outbound passengers | 3300 | - | J-IDEA
Population at risk| 11m | - | J-IDEA
Initial cases     | 1 |	- |	Andersen
Introduction date |	2019-11-22 | - | Andersen
Proportion of cases with onsets known |	0.16 | - |Xu et al
Relative reporting outside of Wuhan | 83 | - | Estimated
Transmission volatility | 0.5 | - | Estimated

# Biblio

[Abbot2020](https://cmmid.github.io/topics/covid19/current-patterns-transmission/global-time-varying-transmission.html) Temporal variation in transmission during the COVID-19 outbreak

[EquipeETE2020a](http://alizon.ouvaton.org/Rapport1_R0_France.html) Rapport : Estimation du nombre de reproduction de l’épidémie de COVID-19 en France

[EquipeETE2020b](http://alizon.ouvaton.org/Report3_Model.html) Modelling the COVID-19 epidemics: an SEAIR model

[Ferguson2020](https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-NPI-modelling-16-03-2020.pdf) Impact of non-pharmaceutical interventions (NPIs) to reduce COVID-19 mortality and healthcare demand

[Flaxman2020](https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-Europe-estimates-and-NPI-impact-30-03-2020.pdf) Estimating   the   number   of   infections   and   the   impact   of   non-pharmaceutical interventions on COVID-19 in 11 European countries
avec un [repo Github](https://github.com/ImperialCollegeLondon/covid19model)

[Hellewell2020](https://www.medrxiv.org/content/medrxiv/early/2020/02/11/2020.02.08.20021162.full.pdf)Feasibility of controlling 2019-nCoV outbreaks by isolation of cases and contacts

[Kucharsky2020a](https://cmmid.github.io/topics/covid19/current-patterns-transmission/wuhan-early-dynamics.html)Analysis and projections of transmission dynamics of nCoV in Wuhan

[Kucharsky2020b](https://www.thelancet.com/journals/laninf/article/PIIS1473-3099(20)30144-4/fulltext)Early dynamics of transmission and control of COVID-19: a mathematical modelling study

[Li2020](https://www.nejm.org/doi/10.1056/NEJMoa2001316)Early Transmission Dynamics in Wuhan, China, of Novel Coronavirus–Infected Pneumonia

[Prem2020](The effect of control strategies to reduce social mixing on outcomes of the COVID-19 epidemic in Wuhan, China: a modelling study)The effect of control strategies to reduce social mixing on outcomes of the COVID-19 epidemic in Wuhan, China: a modelling study

[Wu2020](https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(20)30260-9/fulltext)Nowcasting and forecasting the potential domestic and international spread of the 2019-nCoV outbreak originating in Wuhan, China: a modelling study

[Zhou2020]( t) Clinical course and risk factors for mortality of adult inpatients with COVID-19 in Wuhan, China: a retrospective cohort study

# Data

[Coronavirus Source Data](https://ourworldindata.org/coronavirus-source-data) (Oxford ?) compilation de données WHO et ECDC

[Parameter estimation](https://github.com/midas-network/COVID-19/tree/master/parameter_estimates/2019_novel_coronavirus) MIDAS computable parameter estimate listing.

# Packages

[R0](https://cran.r-project.org/web/packages/R0/) R0: Estimation of R0 and Real-Time Reproduction Number from Epidemics

[SEIR_COVID19](https://github.com/alsnhll/SEIR_COVID19/tree/master/COVID19seir) Code de l'application [https://alhill.shinyapps.io/COVID19seir/](https://alhill.shinyapps.io/COVID19seir/)

[Modèle SEIR](https://gabgoh.github.io/COVID/)

[Modèle de Kucharsky2020b](https://github.com/adamkucharski/2020-ncov) Répertoire du papier [Kucharsky2020b]

[Modèle de Hellewell2020](https://github.com/epiforecasts/ringbp.git) Répertoire du papier [Hellewell2020]