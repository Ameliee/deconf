/* The state object : a container for all global variables (data, pointers to DOM, current states, etc   */

state = {
    /* -------- layout objects --------- */
    mymap: L.map('tester', { zoomControl: false}),/* The leaflet object containing the map */

    markers: [], /*an array of all the map markers that display measures. */
    
    /* -------- data objects ----------*/
    request_data: Object(), /* object to pass requests to the model */
    result_data: Object(), /* object that stores the current results of the model */
    initial_request_data : Object(), initial_result_data: Object(), /* backup for the aforementioned */

    index_regions : Object(), /* an index that maps the iso code of a region to its coordinate in the request_data and result_data objects*/
    
    initial_conditions: Object(), /* The initial conditions and parameters for the model */
    optim_params: Object(), /*The parameters for the optimizer*/
    
    data_age: Object(), /* plotly data age distribution in regions */
    data_emploi: Object(), /* plotly data work branch distribution in regions */

    geojson: Object(), /* the leaflet container for the json data of regions */ 
    
    bar_layout: {
	font: {size:10, color:'#ffffff'},
	margin: {
            l: 30,
            r: 20,
            b: 60,
            t: 10,
            pad: 4
        },
        paper_bgcolor:'rgba(0,0,0,0)',
        plot_bgcolor:'rgba(0,0,0,0)',
        dragmode:false	
    },/* The plotly object showing graphs of region age and work distributions */
    
    xdata:[], ydata_cases:[], ydata_gdb:[], ydata_deaths:[], /* timeline plotly data */
    
    /* -------- current state objects ---------- */
    selected_region: "none", /* Which region is currently selected */
    current_time : 0, /* which week is currently selected */
    selected_graph_to_draw:"cases",
    input_mode:"regional" /*how to modify the strategy : "regional"-y or "national"-y */
}

/* compute internal state fields */
function init_state(){
  
    /* all these fields are hard written by the server in the index.html file */
    state.request_data = initial_data.request_data;
    state.result_data = initial_data.result_data;
    state.initial_conditions = initial_data.initial_conditions;
    state.optim_params = initial_data.optim_params;

    /* deep copy for backup */
    state.initial_request_data = JSON.parse(JSON.stringify(state.request_data));
    state.initial_result_data = JSON.parse(JSON.stringify(state.result_data));
    
    /* compute the region indices */
    state.index_regions = {};
    for(var i = 0; i<state.request_data.zones.length; ++i){state.index_regions[state.request_data.zones[i]] = i;}

 
}

/* ############################      Map utilities     ######################### */

/* initialise the map object */
function init_map(){
    state.mymap.keyboard.disable(); /* we want the + and - keys to control the timeline, not the map */

    /*call server to get all region contents (including age and workforce) */
    jQuery.getJSON("/deconf/geoData.geojson", function (data){
	state.geojson = L.geoJSON(data, {style: style, onEachFeature: onEachFeature}).addTo( state.mymap);
	state.geojson['data'] = data
	state.mymap.fitBounds(state.geojson.getBounds());
	refresh_map();
	state.geojson.bindPopup(function(layer) {
	    return getPopupContent(layer.feature);
	})
    });
}

/* callback to display the border of regions when hovered upon */
function highlightFeature(e) {
    var layer = e.target;
    current_style = style(e)
    current_style.color = "#444444";
    layer.setStyle(current_style);
    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) layer.bringToFront();
}

/* callback for what to do when the hovering stops */
function resetHighlight(e) {
    state.geojson.resetStyle(e.target);
}


/* callback for what to do when a region is clicked */
function displayFeature(e) {
    var layer = e.target;
    state.selected_region = layer.feature.properties.iso_reg; 

    /* change what is displayed in the region tabs */
    $("#nomregion").text(layer.feature.properties.region1 +
			 " ("+layer.feature.properties.code_region+")");
    $("#popregion").text(Math.round(layer.feature.properties.pop).toLocaleString('fr-FR'));
    $("#popactiveregion").text(Math.round(layer.feature.properties.pop_active).toLocaleString('fr-FR'));

    /* refresh everything */
    populate_age_work_graphs(layer)
    refresh_bar();
    refresh_input();
}

/* callback to set the content of the popups on regions */
function getPopupContent(feature){
    reg = state.index_regions[feature.properties.iso_reg]
    var s1,s2,s3;
    if(state.current_time == 0){
	s1 = Math.round(state.result_data.cases[0].initial[reg]);
	s2 = Math.round(state.result_data.ICU_beds[0].initial[reg]);
	s3 = Math.round(state.result_data.deaths[0].initial[reg]);
    }
    else{
	s1 = Math.round(state.result_data.cases[state.current_time].week[reg]);
	s2 = Math.round(state.result_data.ICU_beds[state.current_time].week[reg]);
	s3 = Math.round(state.result_data.deaths[state.current_time].week[reg]);
    }
    s = '<p class="text-center" style="font-size:12px; margin-top:2px; margin-bottom:2px">Contaminés: ' + s1 + '</p>'
    s += '<p class="text-center" style="font-size:12px; margin-top:2px; margin-bottom:2px">En soins intensifs: ' + s2 + '</p>'
    s += '<p class="text-center" style="font-size:12px; margin-top:2px; margin-bottom:2px">Morts: ' + s3 + '</p>'

    return feature.properties.region1 + s
}

/*callback for what happens on each feature */
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: displayFeature
    });
}

/* color map function to draw data on the map */
function getColorMap(d, data) {
    region_cases = data[0].initial.slice();
    for(var i = 1; i<data.length; ++i){
	region_cases = region_cases.concat(data[i].week);
    }
    
    maxCases = Math.max(...region_cases);
    
    return d > 7*maxCases/8 ? '#800026' :
        d > 6*maxCases/8  ? '#BD0026' :
        d > 5*maxCases/8  ? '#E31A1C' :
        d > 4*maxCases/8  ? '#FC4E2A' :
        d > 3*maxCases/8   ? '#FD8D3C' :
        d > 2*maxCases/8   ? '#FEB24C' :
        d > 1*maxCases/8   ? '#FED976' :
        '#FFEDA0';
}

/* styling callback for how to draw the map region color */
function style(feature){
    var data;
    if(state.current_graph_to_draw == "cases"){
	data = state.result_data.cases
    }
    else{
	return {
            fillColor: '#FFEDA0',
            weight: 2,
            opacity: 1,
            color: "#32a1ce",
            dashArray: '3',
            fillOpacity: 0.7
	};
    }
    var color;
    if(state.current_time == 0){
	color = getColorMap(data[current_time].initial[index_regions[feature.properties.iso_reg]], data);
    }
    else{
	color = getColorMap(data[current_time].week[index_regions[feature.properties.iso_reg]], data);
    }
    return {
        fillColor: color,
        weight: 2,
        opacity: 1,
        color: "#32a1ce",
        dashArray: '3',
        fillOpacity: 0.7
    };
}

/* function to draw the leaflet map */
function refresh_map(){
    //TODO : select the data to plot (cases, deaths, etc)
    state.geojson.setStyle(style);
    keyframe = get_keyframe();
    last_week = state.request_data.measure_weeks.indexOf(keyframe);
    
    for(var i = 0; i<state.markers.length; i++){
	state.markers[i].remove();
    }
    state.markers = [];
    
    var icon_size = [32,32];

    var WorkIcon = L.Icon.extend({
	options: {
	    iconUrl : "/static/iconeTravail_border.png",
            iconSize:     icon_size,
            iconAnchor:   [30, 6],
            popupAnchor:  [-15, 0]
	}
    });
    work_icon = new WorkIcon();

    var SocialContainmentIcon = L.Icon.extend({
	options: {
	    iconUrl : "/static/loisir_border.png",
            iconSize:     icon_size,
            iconAnchor:   [0, 30],
            popupAnchor:  [20, -30]
	}
    });
    social_icon = new SocialContainmentIcon();

    var SchoolIcon = L.Icon.extend({
	options: {
	    iconUrl : "/static/schooling_border.png",
            iconSize:     icon_size,
            iconAnchor:   [30, 30],
            popupAnchor:  [-15, -30]
	}
    });
    school_icon = new SchoolIcon();

    var OldIcon = L.Icon.extend({
	options: {
	    iconUrl : "/static/old_border.png",
            iconSize:     icon_size,
            iconAnchor:   [0, 0],
            popupAnchor:  [20, 0]
	}
    });
    old_icon = new OldIcon();
    
    for(var i = 0; i<state.geojson.data.features.length; ++i){
	reg = state.index_regions[state.geojson.data.features[i].properties.iso_reg];
	if(state.current_time == 0 || state.request_data.school_closures[last_week].activated[reg]){
	    marker = L.marker(state.geojson.data.features[i].properties.geo_point_2d, {icon:school_icon})
	    marker.bindPopup('Fermeture des écoles')
	    marker.addTo(state.mymap)
	    state.markers.push(marker);
	}
	if(state.current_time == 0 || state.request_data.forced_telecommuting[last_week].activated[reg]){
	    marker = L.marker(state.geojson.data.features[i].properties.geo_point_2d, {icon:work_icon})
	    marker.bindPopup('Télétravail')
	    marker.addTo(state.mymap)
	    state.markers.push(marker);
	}
	if(state.current_time == 0 || state.request_data.social_containment_all[last_week].activated[reg]){
	    marker = L.marker(state.geojson.data.features[i].properties.geo_point_2d, {icon:social_icon})
	    marker.bindPopup('Confinement social')
	    marker.addTo(state.mymap)
	    state.markers.push(marker);
	}
	if(state.current_time == 0 || state.request_data.total_containment_high_risk[last_week].activated[reg]){
	    marker = L.marker(state.geojson.data.features[i].properties.geo_point_2d, {icon:old_icon})
	    marker.bindPopup('Confinement des personnes à risque')
	    marker.addTo(state.mymap)
	    state.markers.push(marker);
	}
    }
    
    state.geojson.bindPopup(function(layer) {
	return getPopupContent(layer.feature);
    })
}


/* ################      Regional statistics (bar graph) utilities     ################ */

/* fill data for the plotly graphs to be later rendered */
function populate_age_work_graphs(layer) {
    /* age data for the selected region */
    state.data_age = {
	type: 'bar',
	x:[],
	y:[],
	marker: {
            color: '#32a1ce',
            line: {
		width: 2
            }
	}
    };
    for (var age in layer.feature.properties.age){
	state.data_age.x.push(age.toString() + "-" + (parseInt(age)+4).toString() + " ans");
	state.data_age.y.push(layer.feature.properties.age[age])
    }

    /* workforce data for the selected region */
    state.data_emploi = {
	type: 'bar',
	x:[],
	y:[],
	marker: {
            color: '#32a1ce',
            line: {
		width: 2.5
            }
	}
    };
    for (var emploi in layer.feature.properties.emploi){
	state.data_emploi.x.push(emploi.toString());
	state.data_emploi.y.push(layer.feature.properties.emploi[emploi])
    }
}


/* Initialisation of the bar graphs */
function init_bar(){
    /* Refresh when switching between age and workforce bar graphs */
    $('#age-tab').on('shown.bs.tab', function (e) {
	refresh_bar();
    })
    $('#emploi-tab').on('shown.bs.tab', function (e) {
	refresh_bar();
    })

    $("#regional-tab").on('shown.bs.tab', function (e) {
	state.input_mode = "regional";
	refresh_input();
    })
    $('#national-tab').on('shown.bs.tab', function (e) {
	state.input_mode = "national";
	refresh_input();
    })
}


/* Function to call to refresh the bar graphs for region statistics */
function refresh_bar(){
    if( $('#age-tab').hasClass('active')){ /*check which tab is active (maybe ugly : optimise later ?) */
        Plotly.newPlot('bargraph', [state.data_age], state.bar_layout, {responsive:true, displayModeBar: false})
    }
    else if( $('#emploi-tab').hasClass('active')){
        Plotly.newPlot('bargraph', [state.data_emploi], state.bar_layout, {responsive:true, displayModeBar: false})
    }
}

/* #############   Strategy modification utilities (a.k.a. "input") ##################   */

/* Button utilities */
function init_input(){
    $("#input-sauver").on('click', function(e){
	save_new_measure();
    });

    $("#input-annuler").on('click', function(e){
	refresh_input();
    });

    $("#input-relancer").on("click", function(e){
	run_simulation()
    });
    

    $("#input-reset").on('click', function(e){
	state.request_data = state.initial_request_data;
	state.result_data = state.initial_result_data;
	state.current_time=0;
	refresh_input();
	refresh_timeline_with_data();
    });
}


/* Find the last point with a strategy change in regards to the current time */
function get_keyframe(){
    if(state.current_time < state.request_data.measure_weeks[0]) return 0;
    indices = state.request_data.measure_weeks.filter(function(i) {return i<=state.current_time});
    return indices[indices.length-1]
}

/* utility to directly set all input checkboxes to disabled or not */
function set_input_checkboxes_disabled(disabled){
    if(disabled){
	$("#form-school-closure").prop("disabled", true)
	$("#form-social-containment").prop("disabled", true)
	$("#form-telecommuting").prop("disabled", true)
	$("#form-risk-containment").prop("disabled", true)
    }
    else{
	$("#form-school-closure").removeAttr("disabled")
	$("#form-social-containment").removeAttr("disabled")
	$("#form-telecommuting").removeAttr("disabled")
	$("#form-risk-containment").removeAttr("disabled")
    }
}

/* Refresh everything after the user changed something in the input (or cancel) */
function refresh_input(){
    keyframe = get_keyframe();
    last_week = state.request_data.measure_weeks.indexOf(keyframe)
    if(state.current_time==0){
	/* Initial state cannot be modified */
	$("#input-sauver").prop('disabled',true)
	$("#input-annuler").prop('disabled',true)
	set_input_checkboxes_disabled(true)
    }
    else{
	$("#input-sauver").removeAttr("disabled")
	$("#input-annuler").removeAttr("disabled")
    }

    if(state.input_mode=="regional"){
	if(state.selected_region == "none"){/* If no region is selected, nothing can be changed in regional mode */
	    set_input_checkboxes_disabled(true);
	}
	else{
	    reg = state.index_regions[state.selected_region];
	    
	    if(state.current_time!=0){
		set_input_checkboxes_disabled(false);
	    }
	    if(state.current_time<state.request_data.measure_weeks[0]){
		$("#form-school-closure").prop("checked", true);
		$("#form-social-containment").prop("checked", true);
		$("#form-telecommuting").prop("checked", true);
		$("#form-risk-containment").prop("checked", true);
	    }
	    else{
		$("#form-school-closure").prop("checked",state.request_data.school_closures[last_week].activated[reg]);
		$("#form-social-containment").prop("checked",state.request_data.social_containment_all[last_week].activated[reg]);
		$("#form-telecommuting").prop("checked",state.request_data.forced_telecommuting[last_week].activated[reg]);
		$("#form-risk-containment").prop("checked",state.request_data.total_containment_high_risk[last_week].activated[reg]);
	    }
	}
    }
    else{//national mode : TODO 
	if(state.current_time!=0){
	    set_input_checkboxes_disabled(false);
	}
	$("#form-school-closure").removeAttr("checked")
	for(var reg=0; reg<state.index_regions.length; ++reg){
	    if(!state.request_data.school_closures[last_week].activated[reg]) $("#form-school-closure").prop("checked",false)
	}
	$("#form-social-containment").removeAttr("checked")
	for(var reg=0; reg<state.index_regions.length; ++reg){
	    if(!state.request_data.social_containment_all[last_week].activated[reg]) $("#form-social-containment").prop("checked",false)
	}
	$("#form-telecommuting").removeAttr("checked")
	for(var reg=0; reg<state.index_regions.length; ++reg){
	    if(!state.request_data.forced_telecommuting[last_week].activated[reg]) $("#form-telecommuting").prop("checked",false)
	}
	$("#form-risk-containment").removeAttr("checked")
	for(var reg=0; reg<state.index_regions.length; ++reg){
	    if(!state.request_data.total_containment_high_risk[last_week].activated[reg]) $("#form-risk-containment").prop("checked",false)
	}
    }
}

/* What happens when you try to add a new measure at the selected week */
function save_new_measure(){
    if(!state.request_data.measure_weeks.includes(state.current_time)){
	key = get_keyframe();
	
	/*insert new keyframe at the right spot*/
	state.request_data.measure_weeks.splice(key,0,state.current_time)
	if(key==0){
	    dummy = []
	    for(var i=0; i<state.request_data.zones.length; ++i) dummy.push(true); 
	    state.request_data.school_closures.unshift(dummy.slice())
	    state.request_data.social_containment_all.unshift(dummy.slice())
	    state.request_data.forced_telecommuting.unshift(dummy.slice())
	    state.request_data.total_containment_high_risk.unshift(dummy.slice())
	}
	else{
	    last_week = state.request_data.measure_weeks.indexOf(key)
	    state.request_data.school_closures.splice(last_week+1,0,
						      {"activated":state.request_data.school_closures[last_week].activated.slice()})
	    state.request_data.social_containment_all.splice(last_week+1,0,
							     {"activated":state.request_data.social_containment_all[last_week].activated.slice()})
	    state.request_data.forced_telecommuting.splice(last_week+1,0,
							   {"activated":state.request_data.forced_telecommuting[last_week].activated.slice()})
	    state.request_data.total_containment_high_risk.splice(last_week+1,0,
								  {"activated":state.request_data.total_containment_high_risk[last_week].activated.slice()})
	}
    }
    if(state.input_mode=="regional"){
	reg = state.index_regions[state.selected_region]
	key = state.request_data.measure_weeks.indexOf(state.current_time)
	
	state.request_data.school_closures[key].activated[reg] = $("#form-school-closure").prop("checked")
	state.request_data.social_containment_all[key].activated[reg] = $("#form-social-containment").prop("checked")
	state.request_data.forced_telecommuting[key].activated[reg] = $("#form-telecommuting").prop("checked")
	state.request_data.total_containment_high_risk[key].activated[reg] = $("#form-risk-containment").prop("checked")
    }
    else{//TODO: change only modified
	key = state.request_data.measure_weeks.indexOf(state.current_time)
	for(var reg=0; reg<state.request_data.zones.length; ++reg){ 
	    state.request_data.school_closures[key].activated[reg] = $("#form-school-closure").prop("checked")
	    state.request_data.social_containment_all[key].activated[reg] = $("#form-social-containment").prop("checked")
	    state.request_data.forced_telecommuting[key].activated[reg] = $("#form-telecommuting").prop("checked")
	    state.request_data.total_containment_high_risk[key].activated[reg] = $("#form-risk-containment").prop("checked")
	}
    }
    
    refresh_input();
    refresh_map();
}



function run_simulation(){
    $.ajax({
	type: "POST",
	url: "/deconf/relaunch_model",
	data: JSON.stringify(state.request_data),
	contentType: "application/json; charset=utf-8",
	dataType: "json",
	success: function(data){
	    state.result_data=data;
	    refresh_timeline_with_data();
	    refresh_map();
	},
	failure: function(errMsg) {
            alert(errMsg);
	}
    });
    
}

/* ################## Timeline utilities ################## */

/* Initialise timeline */
function init_timeline(){
    /* keyboard shortcuts */
    $(document).on('keypress',function(e) {
	if(e.which == 43) {
	    ++state.current_time;
	    redraw_cursor();
	    refresh_map();
	    refresh_input()
	}
	if(e.which == 45) {
	    --state.current_time;
	    if(state.current_time<0) state.current_time=0;
	    redraw_cursor();
	    refresh_map();
	    refresh_input();
	}
    });

    refresh_timeline_with_data();
}

/* draw the current time on the timeline */
function redraw_cursor(){
    update = {
	shapes: [
	    {
		type:'rect',
		xref:'x',
		yref:'y',
		x0:state.current_time-0.5,
		y0:1.1*Math.min(Math.min(...state.ydata_cases),Math.min(...state.ydata_gdb)),
		x1:state.current_time+0.5,
		y1:1.1*Math.max(Math.max(...state.ydata_cases),Math.max(...state.ydata_gdb)),
		opacity: 0.8,
		fillcolor:'#32a1c2',
		line:{
		    color:'#32a1c2'
		}
	    }
	]
    };
    Plotly.relayout('frise', update);
    $("#semaine-affiche").text("Semaine "+state.current_time.toString());
}

/* function to call when the timeline data has been changed */
function refresh_timeline_with_data(){
    state.xdata = [...Array(state.result_data.cases.length).keys()];
    state.ydata_cases = [state.result_data.cases[0].initial.reduce(function(a,b){return a+b;})]; 
    for(var l=0; l<state.result_data.cases.length-1; ++l){
	state.ydata_cases.push(state.result_data.cases[l+1].week.reduce(function(a,b){return a+b;}));
    }
    state.ydata_deaths = [state.result_data.deaths[0].initial.reduce(function(a,b){return a+b;})];
    for(var l=0; l<state.result_data.deaths.length-1; ++l){
	state.ydata_deaths.push(state.result_data.deaths[l+1].week.reduce(function(a,b){return a+b;}));
    }
    state.ydata_gdb = [];
    for(var l=0; l<state.result_data.cases.length; ++l){
	state.ydata_gdb.push(state.result_data.gdp_accumulated_loss[l]);
    }
    refresh_timeline();
}

/* Redraw the timeline, for instance when the current time is updated */
function refresh_timeline(){
    timeline_plot = Plotly.newPlot('frise', {
	data: [
	    {
		type: 'scatter',
		name: 'Contaminés',
		x: state.xdata,
		y: state.ydata_cases,
		line: {
		    color: 'red',
		    simplify: false,
		}
	    },
	    {
		type: 'scatter',
		name: 'Economie',
		x: state.xdata,
		y: state.ydata_gdb,
		yaxis: 'y2',
		line: {
		    color: 'orange',
		    simplify: false,
		}
	    },
	    {
		type: 'scatter',
		name: 'Morts',
		x: state.xdata,
		y: state.ydata_deaths,
		yaxis: 'y1',
		line: {
		    color: 'yellow',
		    simplify: false,
		}
	    }],
	layout: {
	    paper_bgcolor:'rgba(0,0,0,0)',
	    plot_bgcolor:'rgba(0,0,0,0)',
	    margin: {
		l: 60,
		r: 20,
		b: 20,
		t: 10
	    },
	    xaxis: {tickmode: 'linear', title:'semaine',tick0:0,dtick:10,ntick:state.xdata.length, automargin:true},
	    yaxis: {title:'Nb de personnes'},
	    yaxis2: {title: 'Points de PIB &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', side:'right', overlaying:'y'},
	    dragmode:false,
	    hovermode:'closest',
	},
	config: {responsive:true, displayModeBar: false}
    });
    redraw_cursor();
    
    $("#frise").on('plotly_click', function(_,data){
	state.current_time = data.points[0].x
	redraw_cursor();
	refresh_map();
	refresh_input()
    });
}

/* ##################     Simulation utilities    ################# */

function init_simulation(){
    $("#initial-valider").on("click", function(e){
	for(field in state.request_data.compartmental_model_parameters){
	    state.request_data.compartmental_model_parameters[field] = JSON.parse($("#"+field).val());
	}
	run_simulation()
    });	

    $("#initial-annuler").on("click", function(e){
	for(field in state.request_data.compartmental_model_parameters){
	    $("#"+field).val(JSON.stringify(state.request_data.compartmental_model_parameters[field]))
	}
    });
}


/* ##################     Optimisation utilities    ################# */
function init_optim(){
    $("#optim-valider").on("click", function(e){


	for(field in state.optim_params){
	    if(state.request_data.hasOwnProperty(field)){/* copy initial conditions and zones */
		state.optim_params[field] = state.request_data[field];
	    }
	    else if(field == "max_measure_weeks"){
		state.optim_params[field] = parseInt($("#"+field).val())
	    }
	}
	for(field in state.optim_params.activable_measures){
	    console.log(field)
	    console.log("#"+field)
	    state.optim_params.activable_measures[field] = $("#"+field).prop('checked')
	}

	console.log(JSON.stringify(state.optim_params))
	
	$.ajax({
	    type: "POST",
	    url: "/deconf/optimize",
	    data: JSON.stringify(state.optim_params),
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function(data){
		/* split the request into req (measures) and res (modeling output) */
		for(field in data){
		    if(state.request_data.hasOwnProperty(field)) state.request_data[field]= JSON.parse(JSON.stringify(data[field]))
		    if(state.result_data.hasOwnProperty(field)) state.result_data[field]= JSON.parse(JSON.stringify(data[field]))
		}
		dummy_false = [false];
		for(var j=0; j<state.request_data.zones.length; ++j) dummy_false.push(false);
		dummy_true = [true];
		for(var j=0; j<state.request_data.zones.length; ++j) dummy_true.push(true);
		
		for(field in state.request_data){
		    if(state.request_data[field] == null){ /* when the optimizer returns a null array, fill it with coherent data. */
			state.request_data[field] = [{"activated" : dummy_true.slice()}]
			for(var j = 1; j<state.request_data.measure_weeks.length; ++j){
			    state.request_data[field].push({"activated" : dummy_false.slice()})
			}
		    }
		}
		
		console.log(state.request_data)
		refresh_timeline_with_data();
		refresh_input();
		refresh_map();
	    },
	    failure: function(errMsg) {
		alert(errMsg);
	    }
	}); 
    });
    
    $("#optim-annuler").on("click", function(e){
	$("#max_measure_weeks").prop("checked", state.optim_params.max_measure_week)
	for(field in state.optim_params){
	    if(field != "zones" && field != "max_measure_week")
		$("#"+field).prop("checked", state.optim_params[field])
	} 
    });
}



function init_rendering(){
    init_state();
    init_map();
    init_bar()
    init_input();
    init_timeline();
    init_simulation();
    init_optim();
}

init_rendering();
