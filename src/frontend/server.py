import csv,os,sys

import sqlite3, time, json
from flask import Flask, render_template, request, escape, url_for, session, g, redirect, abort, jsonify

from werkzeug.security import check_password_hash, generate_password_hash

import requests

app = Flask(__name__)

import json, random

def format_datetime(value):
    s = value % 60
    m = value // 60
    h = m // 60
    m = m % 60
    d = h // 60
    h = h % 60
    w = d // 7
    d = d % 7
    if w != 0:
        return "%dsem %dj %dh %dm %ds"%(w,d,h,m,s)
    elif d!=0:
        return "%dj %dh %dm %ds"%(d,h,m,s)
    elif h!=0:
        return "%dh %dm %ds"%(h,m,s)
    elif m!=0:
        return "%dm %ds"%(m,s)
    else:
        return '%ds'%(s)

app.jinja_env.filters['datetime'] = format_datetime

@app.route('/deconf/', methods=['GET'])
def index():
    if request.method=='GET':
        with open('static/eval_policy_req.json') as fp:
            req = json.load(fp)
        with open('static/eval_policy_res.json') as fp:
            res = json.load(fp)
        with open('static/optimize_req.json') as fp:
            optimize_req = json.load(fp)

        measure_names = { "forced_telecommuting":"Télétravail imposable",
                          "school_closures":"Ecoles fermables",
                          "social_containment_all":"Confinement social possible",
                          "total_containment_high_risk":"Confinement des personnes à risque"}
    
        return render_template('index.html', res=json.dumps(res), req=json.dumps(req), 
                               optim_params = optimize_req,
                               optim_params_json = json.dumps(optimize_req),
                               measure_names = measure_names,
                               autoescape=False)

def add_csv_fields(data, index, filename, fieldname, factor=1):
    with open(filename, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        
        for row in csv_reader:
            key = row['region']
            del row['region']
            if key in index:
                data['features'][index[key]]['properties'][fieldname] =\
                    {k:factor*float(v) for (k,v) in row.items()}

codes = {"AUVERGNE-RHONE-ALPES":"FR-ARA",
         "BOURGOGNE-FRANCHE-COMTE":"FR-BFC",
         "BRETAGNE":"FR-BRE",
         "CENTRE-VAL DE LOIRE":"FR-CVL",
         "CORSE":"FR-COR",
         "GRAND EST":"FR-GES",
         "HAUTS-DE-FRANCE":"FR-HDF",
         "ILE-DE-FRANCE":"FR-IDF",
         "NORMANDIE":"FR-NOR",
         "NOUVELLE-AQUITAINE":"FR-NAQ",
         "OCCITANIE":"FR-OCC",
         "PAYS DE LA LOIRE":"FR-PDL",
         "PROVENCE-ALPES-COTE D'AZUR":"FR-PAC",
         "GUADELOUPE":"FR-GP",
         "GUYANE":'FR-GF',
         'MARTINIQUE':'FR-MQ',
         'LA REUNION':'FR-RE',
         'MAYOTTE':'FR-YT'}
                
def add_json_fields(data,index, filename, fieldname): 
    with open(filename) as fp:
        d = json.load(fp)
        for reg in index:
            data['features'][index[reg]]['properties'][fieldname]=d[codes[reg]]
            
def add_sum_fields(data, index, field_dict, field_sum): 
    for reg in index:
        data['features'][index[reg]]['properties'][field_sum] =\
            sum(data['features'][index[reg]]['properties'][field_dict].values())

        
@app.route('/deconf/geoData.geojson', methods = ['GET'])
def geoData():
    if request.method=='GET':
        with open('static/regions.geojson') as f:
            data = json.load(f)
            toremove = ["MAYOTTE",
                        "LA REUNION",
                        "GUYANE",
                        "MARTINIQUE",
                        "GUADELOUPE"]
            data['features'] = [a for a in data['features'] if not a['properties']['region'] in toremove]
            index = {a['properties']['region']:i for (i,a) in enumerate(data['features'])}
            for reg in index:
                data['features'][index[reg]]['properties']['iso_reg'] = codes[reg]
            add_json_fields(data, index, 'static/data_emploi_region.json', 'emploi')
            add_sum_fields(data, index, 'emploi', 'pop_active')
            add_json_fields(data, index, 'static/data_age_region.json', 'age')
            add_sum_fields(data, index, 'age', 'pop')
            
        return json.dumps(data)  
    else:
        return ""
    

def get_model_path(cmd):
    model_host = os.environ.get('MODEL_HOST')
    
    if model_host is None or model_host=="":
        model_host="localhost"
    model_port = os.environ.get('MODEL_PORT')
    if model_port is None or model_port=="":
        model_port = 8000
    else:
        model_port = int(model_port)
    return 'http://%s:%d/%s'%(model_host,model_port, cmd)

@app.route('/deconf/relaunch_model', methods=['GET', 'POST'])
def relaunch_model():
    if request.json is not None:
        print(request.json)
        a = requests.post(get_model_path('eval_policy'),
                          data=json.dumps(request.json))
        return json.dumps(a.json())
    else:
        return "toto"

def get_optim_path():
    optim_host = os.environ.get('OPTIM_HOST')
    if optim_host is None or optim_host=="":
        optim_host="localhost"
    optim_port = os.environ.get('OPTIM_PORT')
    if optim_port is None or optim_port=="":
        optim_port = 8001
    else:
        optim_port = int(optim_port)
    return 'http://%s:%d/optimize'%(optim_host,optim_port)


@app.route('/deconf/optimize', methods=['GET', 'POST'])
def optimize(): 
    if request.json is not None:
        a = requests.post(get_optim_path(), data=json.dumps(request.json))
        return json.dumps(a.json())
    else:
        return "toto"
