TODOLIST

Fonctions

- améliorer "responsivité" (diminuer la taille de la carte sur les petits écrans) -> premier step mais à améliorer
- Ajouter les DOM/TOM sur le bord de la carte
- Gérer un état intermédiaire dans le mode d'édition "national"
- Ajouter le visualisation/suppression de keyframes
- Changer les icones avec des choses plus explicites
- Ajouter la possibilité de changer les mesures sur une plage de temps (t+k semaines)
- Sélectionner/Déselectionner tout d'un coup dans les mesures 
- Annulation d'une action
- Plus de raccourcis clavier
- Ajouter un bouton play pour éviter d'avoir à maintenir appuyé '+'


Code

- /!\ Refactorer tout le code+html/css pour améliorer la propreté + maintenabilité
- améliorer la structure des dossiers pour ne pas avoir les assets dans le dossier src
- rendre l'affichage des paramètres en entrée du simulateur moins hard-coded par rapport aux champs de l'API