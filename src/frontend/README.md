Dépendances notables (ajoutées à "requirements.txt"):

  - python
  - flask (pip install Flask)

Lancer le serveur pour le modele:

  - Cf README de src/model

Lancer en local (dans le dossier frontend):

  - sh run_local.sh

NB : cela lance automatiquement une requete d'initialisation des paramètres vers
le serveur du modèle (avec les paramètres trouvés dans le répertoire doc)

Pour consulter la page dans le navigateur :

- localhost:8080/deconf


Pour bidouiller avec les données : il suffit de changer les json du dossier static.
Attention que la commande run_local.sh fait des copies des données dedans
