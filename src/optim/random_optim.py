# !/usr/bin/env python3
from copy import deepcopy
from random import randint

from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler


class RandomOptimRequestHandler(BaseOptimRequestHandler):

    def optimize(self, optimize_req, policy_common_values, activable_measures_list):
        nzones = len(optimize_req['zones'])
        total_population_size = sum(optimize_req['population_size'])
        best_policy, best_policy_grade = None, None
        for ntry in range(10):
            policy = deepcopy(policy_common_values)
            measure_weeks = []
            for i in range(optimize_req['max_measure_weeks']):
                if i > 0 and randint(0, 100) > 30:
                    continue
                measure_weeks.append(i)
                for m in activable_measures_list:
                    arr = [randint(0, 100) > 50 for _ in range(nzones)]
                    policy[m].append({'activated': arr})
            # lift all measures at the end
            measure_weeks.append(optimize_req['max_measure_weeks'])
            for m in activable_measures_list:
                policy[m].append({'activated': [False] * nzones})
            # send POST
            policy['measure_weeks'] = measure_weeks
            ok, p_results = self.call_eval_policy(policy, activable_measures_list)
            if not ok:
                return False, None, None, None
            # now check if the policy has good results
            death_beg, death_end = p_results['deaths'][0]['initial'], p_results['deaths'][-1]['week']
            death_rate = 100 * (sum(death_end) - sum(death_beg)) / total_population_size  # percents, > 0
            gdp_accumulated_loss = p_results['gdp_accumulated_loss'][-1]  # percents, < 0
            print('death rate: {}, gdp: {}'.format(death_rate, gdp_accumulated_loss))
            policy_grade = gdp_accumulated_loss - death_rate
            # keep the best
            if best_policy is None or best_policy_grade < policy_grade:
                best_policy, best_results, best_policy_grade = policy, p_results, policy_grade

        return True, best_policy, best_results, best_policy_grade


if __name__ == '__main__':
    launch_server(BaseOptimServer, RandomOptimRequestHandler)
