# !/usr/bin/env python3
from src.model.macromodel import ModelRequestHandler, ModelServer
from src.optim.base_optim import launch_server
from src.optim.random_optim import RandomOptimRequestHandler, BaseOptimServer


class EMORequestHandler(ModelRequestHandler, RandomOptimRequestHandler):
    """
    EMO = embedded model optimizer.
    This model embeds both a SEAIR multi-macromodel and an optimizer.
    As such, we don't have to call lots of POST eval_policy requests,
    but use instead the embedded models.
    """

    def call_eval_policy(self, eval_policy_req: dict, activable_measures_list: list):
        # print('EMORequestHandler::call_eval_policy()')
        eval_policy_res = ModelRequestHandler.eval_policy(self, eval_policy_req)
        self.draw_policy(activable_measures_list, eval_policy_req, eval_policy_res)
        return True, eval_policy_res

    def do_POST(self):
        if 'eval_policy' in self.path:
            return ModelRequestHandler.do_POST(self)
        return RandomOptimRequestHandler.do_POST(self)


class EMOServer(ModelServer, BaseOptimServer):

    def __init__(self, server_address, RequestHandlerClass,
                 bind_and_activate=True,
                 eval_host='localhost', eval_port=8000, draw=False):
        ModelServer.__init__(self, server_address, RequestHandlerClass,
                             bind_and_activate=False, server_init=False)
        BaseOptimServer.__init__(self, server_address, RequestHandlerClass,
                                 bind_and_activate=True, server_init=True, draw=draw)


if __name__ == '__main__':
    launch_server(EMOServer, EMORequestHandler)
