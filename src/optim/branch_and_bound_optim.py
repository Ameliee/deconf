# !/usr/bin/env python3
from copy import deepcopy

from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler


class BBOptimRequestHandler(BaseOptimRequestHandler):

    def optimize_zone(self, current_policy, min_item, max_weeks, number_beds, list_measures, current_grade):
        """
        :param data: Optimize request data
        :param current_solution: solution we are going to check.
        :param items_deconfined: List of items that have had a deconfinement.
        :param best_found_grade: best solution found.
        :return: Best solution.
        """
        potential_solutions = {}
        current_best_policy = current_policy
        current_best_grade = current_grade
        for i in range(min_item, max_weeks * len(list_measures)):
            measure = list_measures[i % len(list_measures)]
            week = i / (len(list_measures)) + 1
            policy = current_policy.copy()
            if week not in policy[measure]:
                for measure2 in list_measures:
                    for j in range(week, 0, -1):
                        if j not in policy[measure2].keys():
                            continue
                        policy[measure2][week] = policy[measure2][j]
                        if week < max_weeks:
                            policy[measure2][week + 1] = policy[measure2][week]
                policy['measure_weeks'].append(week)
                if week < max_weeks:
                    policy['measure_weeks'].append(week + 1)
            policy[measure][0][week] = False
            pres = self.call_eval_policy(current_policy)
            if not self.server.total_population_size:
                self.server.total_population_size = sum(pres['population_size'])
            gdp_accumulated_loss = pres['gdp_accumulated_loss'][-1] / 100  # percents, < 0
            icu_beds = pres["ICU_beds"]
            valid_solution = True
            for j in range(i, max_weeks * len(list_measures)):
                if icu_beds[j] > number_beds:
                    valid_solution = False
                    break
            if valid_solution:
                potential_solutions[i] = (policy, gdp_accumulated_loss)
        for i in potential_solutions.keys():
            best_policy, best_grade = self.optimize_zone(potential_solutions[i][0], i + 1, max_weeks, number_beds,
                                                         list_measures, potential_solutions[i][1])
            if best_grade > current_best_grade:
                current_best_policy = best_policy
        return current_best_policy, current_best_grade

    def extract_data_one_zone(self, data, zone_index):
        data_zone = {}
        data_zone['zones'] = [data['zones'][zone_index]]
        for k in data.keys():
            if k != 'zones':
                data_zone[k] = data[k]
        return data_zone

    def optimize(self, optimize_req, policy_common_values, activable_measures_list):
        nzones = len(optimize_req['zones'])
        list_measures = optimize_req['activable_measures'].keys()
        max_measure_weeks = optimize_req['max_measure_weeks']
        for zone_index in range(0, 1):
            data_zone = self.extract_data_one_zone(optimize_req, zone_index)
            print(data_zone)
            policy = deepcopy(policy_common_values)
            policy['measure_weeks'] = [0]
            for measure in list_measures:
                if measure in activable_measures_list:
                    policy[measure] = [{'activated': [True] * nzones}]
                else:
                    policy[measure] = None

            # lift all measures at the end
            policy['measure_weeks'].append(optimize_req['max_measure_weeks'])
            for m in activable_measures_list:
                policy[m].append({'activated': [False] * nzones})

            print('policy:', policy)
            best_policy = policy
            ok, best_results = self.call_eval_policy(policy, activable_measures_list)
            print('best_results:', best_results)
            best_policy_grade = None
            """
            # send POST eval_policy
            pres = self.call_eval_policy(policy)
            if not self.server.total_population_size:
                self.server.total_population_size = sum(pres['population_size'])
            for k in pres["ICU_beds"]:
                print(k)
            print(pres["ICU_beds"])
            death_beg, death_end = pres['deaths'][0]['initial'], pres['deaths'][-1]['week']
            death_rate = 100 * (sum(death_end) - sum(death_beg)) / self.server.total_population_size  # percents, > 0
            gdp_accumulated_loss = pres['gdp_accumulated_loss'][-1] / 100  # percents, < 0
            print('death rate: {}, gdp: {}'.format(death_rate, gdp_accumulated_loss))
            policy_grade = gdp_accumulated_loss - death_rate
            # keep the best
            if best_policy is None or best_policy_grade < policy_grade:
                best_policy, best_results, best_policy_grade = policy, pres, policy_grade
            """
        return True, best_policy, best_results, best_policy_grade


if __name__ == '__main__':
    launch_server(BaseOptimServer, BBOptimRequestHandler)
