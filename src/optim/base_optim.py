# !/usr/bin/env python3
import json
import sys
from http.server import BaseHTTPRequestHandler, HTTPServer

import matplotlib


# matplotlib.use('Qt5Agg')
# print(matplotlib.get_backend())  # 'TkAgg' by default
import matplotlib.pyplot as plt
import numpy as np
import requests

from src.model.macromodel import error_dumps


class BaseOptimRequestHandler(BaseHTTPRequestHandler):
    def optimize(self, optimize_req: dict,
                 policy_common_values: dict,
                 activable_measures_list: list):
        """
        This function should be inherited by children.
        :param optimize_req: the whole "optimized_req" request as a dict
        :param policy_common_values: compulsoory fields, copied from "optimized_req"
                ['zones', 'population_size', 'compartmental_model', 'compartments',
                'initial_ratios', 'compartmental_model_parameters']
                You can deepcopy it as a skeleton for your custom policies.
                Use call_eval_policy() to test a policy.
        :param activable_measures_list:
            ex. [ 'school_closures', 'social_containment_all'  ]
        :return: ok, best_policy, best_results, best_policy_grade
        """
        ok, best_policy, best_results, best_policy_grade = False, None, None, None
        return ok, best_policy, best_results, best_policy_grade

    @staticmethod
    def make_policy_common_values(optimize_req: dict):
        # copy fields from optimize_req that will be common to all policies
        print('optimize_req:', optimize_req)
        policy_common_values = {k: optimize_req[k] for k in
                                ['zones', 'population_size', 'compartmental_model', 'compartments',
                                 'initial_ratios', 'compartmental_model_parameters']}
        activable_measures_list = []
        for m, m_ok in optimize_req['activable_measures'].items():
            policy_common_values[m] = None
            if m_ok:
                policy_common_values[m] = []
                activable_measures_list.append(m)
        return policy_common_values, activable_measures_list

    def write_error_dumps(self, errstr):
        print('error:', errstr)
        self.wfile.write(json.dumps({'error': errstr, 'success': False}).encode())

    def call_eval_policy(self, eval_policy_req: dict, activable_measures_list: list):
        """
        Send a POST request to macropolicy server.
        This function can be overloaded to do somethig else
        (as in EMO that uses an embedded model).
        """
        # print('OptimRequestHandler::call_eval_policy()')
        response = requests.post(self.server.eval_policy_url, data=json.dumps(eval_policy_req))
        if response.status_code != 200:
            return False, error_dumps('call_eval_policy() failed')
        eval_policy_res = response.json()
        self.draw_policy(activable_measures_list, eval_policy_req, eval_policy_res)
        return True, eval_policy_res

    def draw_policy(self, activable_measures_list: list,
                    eval_policy_req: list, eval_policy_res: list):
        if not self.server.draw:
            return
        # now plot death rates, gdps
        max_days = 1 + max(eval_policy_req['compartmental_model_parameters']['time_interval'])
        nweeks, nmeasures, nzones = int(max_days / 7), len(activable_measures_list), len(eval_policy_req['zones'])
        matrix = np.zeros((nmeasures, nzones, nweeks))
        # draw measure heatmap
        for midx, m in enumerate(activable_measures_list):
            if m not in eval_policy_req or eval_policy_req[m] is None:
                continue
            for w in range(nweeks):
                try:
                    mw_idx = eval_policy_req['measure_weeks'].index(w)
                except ValueError:
                    mw_idx = -1
                for z in range(nzones):
                    if w > 0 and mw_idx == -1:  # copy previous week
                        matrix[midx][z][w] = matrix[midx][z][w - 1]
                    elif eval_policy_req[m][mw_idx]['activated'][z]:
                        matrix[midx][z][w] = 1
            axs = self.axs[midx, 0]
            axs.imshow(matrix[midx], cmap='Greens')
            axs.set_xlabel('week nb')
            axs.set_yticks(np.arange(nzones)[1::2])  # We want to show every other tick
            axs.set_yticklabels(eval_policy_req['zones'][1::2])
            axs.set_title(m)
        death_beg, death_end = eval_policy_res['deaths'][0]['initial'], eval_policy_res['deaths'][-1]['week']
        death_rate = 100 * (sum(death_end) - sum(death_beg)) / self.total_population_size  # percents, > 0
        gdp_accumulated_loss = eval_policy_res['gdp_accumulated_loss'][-1]  # percents, < 0
        self.death_rates.append(death_rate)
        self.gdps.append(gdp_accumulated_loss)
        # print(self.death_rates, self.gdps)
        self.axbig.plot(self.death_rates, self.gdps, 'ro')
        self.fig.tight_layout()
        plt.pause(0.1)

    def draw_optim_request(self, optimize_req, activable_measures_list):
        if not self.server.draw:
            return
        self.total_population_size = sum(optimize_req['population_size'])
        self.death_rates, self.gdps = [], []
        nmeasures = len(activable_measures_list)
        # prepaire heatmap
        # https://matplotlib.org/3.1.1/gallery/images_contours_and_fields/image_annotated_heatmap.html
        self.axs = self.fig.subplots(nrows=nmeasures, ncols=2)
        if nmeasures > 1:
            gs = self.axs[0, 1].get_gridspec()
            # remove the underlying axes
            for ax in self.axs[0:, 1]:
                ax.remove()
            self.axbig = self.fig.add_subplot(gs[0:, 1])
        else:
            self.axs = self.axs.reshape((1, 2))
            self.axbig = self.axs[0, 1]
        self.axbig.set_xlabel('death rates (%)')
        self.axbig.set_ylabel('GDP loss (%)')

    def do_POST(self):
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        optimize_req = json.loads(post_body)
        self.send_response(200)
        self.end_headers()
        dumps = {'success': False, 'error': 'command not recognized'}
        if 'optimize' in self.path:
            # check zones are valid and that population_size has the same length
            if 'zones' not in optimize_req or not optimize_req['zones']:
                return self.write_error_dumps('No zones provided.')
            # TODO we should add more sanity checks here
            policy_common_values, activable_measures_list = self.make_policy_common_values(optimize_req)
            if self.server.draw:
                self.fig = plt.figure()
                self.draw_optim_request(optimize_req, activable_measures_list)
            ok, best_policy, best_results, best_policy_grade = self.optimize(optimize_req, policy_common_values,
                                                                             activable_measures_list)
            if not ok:
                return self.write_error_dumps('optimize() failed.')
            if self.server.draw:
                plt.close('all')
            # measure_weeks, forced_telecommuting, social_containment_all, school_closures, total_containment_high_risk
            dumps = {k: best_policy[k] for k in optimize_req['activable_measures'].keys()}
            dumps['measure_weeks'] = best_policy['measure_weeks']
            # add zones, compartmental_model, max_days, gdp_accumulated_loss, population_size, cases, ICU_beds, deaths, new_cases
            dumps.update(best_results)
        self.wfile.write(json.dumps(dumps).encode())


class BaseOptimServer(HTTPServer):
    def __init__(self, server_address, RequestHandlerClass,
                 bind_and_activate=True, server_init=True,
                 eval_host='localhost', eval_port=8000, draw=False):
        # stuff that needs to be stored here
        self.draw = draw
        if self.draw :
            matplotlib.use('TkAgg')
        if eval_host.startswith('http://'):
            self.eval_policy_url = '%s:%d/eval_policy' % (eval_host, eval_port)
        else:
            self.eval_policy_url = 'http://%s:%d/eval_policy' % (eval_host, eval_port)
        # print(self.eval_policy_url)
        if server_init:
            HTTPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate=bind_and_activate)


def launch_server(OptimServer, OptimRequestHandler):
    if len(sys.argv) == 1:
        optim_host = 'localhost'
        optim_port = 8001
        eval_host = 'localhost'
        eval_port = 8000
        draw = False
    elif len(sys.argv) == 4:
        optim_host = ':'.join(sys.argv[1].split(':')[:-1])
        optim_port = int(sys.argv[1].split(':')[-1])
        eval_host = ':'.join(sys.argv[2].split(':')[:-1])
        eval_port = int(sys.argv[2].split(':')[-1])
        draw = (int(sys.argv[3]) > 0)
    else:
        print("usage : python random_optim.py  hostname(default='localhost'):port(default=8001) "
              "eval_hostname(default='localhost'):eval_port(default=8000) draw")
        print("  eval_host, eval_port : the host and port of the model server to run en evaluation")
        print("  draw: pass 1 to draw policies and Pareto front")
        exit(-1)
    # print(eval_host, eval_port, optim_host, optim_port)
    server = OptimServer((optim_host, optim_port),
                         OptimRequestHandler,
                         eval_host=eval_host, eval_port=eval_port,
                         draw=draw)
    print('Starting server at http://%s:%d, draw:%s' % (optim_host, optim_port, draw))
    server.serve_forever()


if __name__ == '__main__':
    launch_server(BaseOptimServer, BaseOptimRequestHandler)
