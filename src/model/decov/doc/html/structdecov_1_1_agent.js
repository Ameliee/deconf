var structdecov_1_1_agent =
[
    [ "Agent", "structdecov_1_1_agent.html#a42eca3cc166c4a173535057d5e64f9b5", null ],
    [ "contagious", "structdecov_1_1_agent.html#a1a4149316d1221724f0337ca2ff0d378", null ],
    [ "has", "structdecov_1_1_agent.html#a4363643541ccbfebc188b39438c32843", null ],
    [ "operator<<", "structdecov_1_1_agent.html#a1ac46c3e8a7ba0c675b746eaf610995e", null ],
    [ "age", "structdecov_1_1_agent.html#aeb57b762144de9624b5c8bd88724a262", null ],
    [ "indexes", "structdecov_1_1_agent.html#a96c21cd581c8777458eaac5106bb6ed2", null ],
    [ "infected", "structdecov_1_1_agent.html#a761cfb540944df80508ffc834f13b2df", null ],
    [ "spotIds", "structdecov_1_1_agent.html#a71d5aed5da103940141f86175af9edff", null ],
    [ "stage", "structdecov_1_1_agent.html#a5b3ed5a67662d4f4b590e087b4759d4d", null ],
    [ "stageBeginDay", "structdecov_1_1_agent.html#ad915965fa3a4097900c00f75f8ec180e", null ]
];