var structdecov_1_1_simulation_state =
[
    [ "addSpot", "structdecov_1_1_simulation_state.html#a7e99387d8c5d7f67887212015302965c", null ],
    [ "allocateTime", "structdecov_1_1_simulation_state.html#a7b0c22ab8f2e9f715109d2ab3dce8e67", null ],
    [ "assignAgent", "structdecov_1_1_simulation_state.html#aee0390d9d76ffa10ef0038d2b17e018a", null ],
    [ "agents", "structdecov_1_1_simulation_state.html#a7c4a4626e3496c81b86850d8d4fd97fd", null ],
    [ "day", "structdecov_1_1_simulation_state.html#a76452acf2f8b34907708ab928d332818", null ],
    [ "indicators", "structdecov_1_1_simulation_state.html#a1b64f20580eea6e165da4d9c32998bd4", null ],
    [ "spots", "structdecov_1_1_simulation_state.html#ae7216e3df8eaf9da248be6072f09dc1a", null ]
];