var structdecov_1_1_simulator =
[
    [ "Simulator", "structdecov_1_1_simulator.html#a8ddcc7d19e083609b189c3110c554fc9", null ],
    [ "allocateTime", "structdecov_1_1_simulator.html#a717c5d440c1dfd237c7e13aa6d88512b", null ],
    [ "runDay", "structdecov_1_1_simulator.html#ade0fafb0fb6f87dbe2a269b5be517629", null ],
    [ "simulateContact", "structdecov_1_1_simulator.html#a2d11f8ea610c6c16036ee0fb0229c906", null ],
    [ "spreadInfection", "structdecov_1_1_simulator.html#aec61cdccba716a33345844521a787db0", null ],
    [ "updateAgentsStages", "structdecov_1_1_simulator.html#abec11c917f40a22cd1e70c3049d85449", null ],
    [ "parameters", "structdecov_1_1_simulator.html#af416d8428051f6e9a44f7570ba13856a", null ],
    [ "policy", "structdecov_1_1_simulator.html#ab3a9f36a67a02331bc65e412343d7bb7", null ],
    [ "state", "structdecov_1_1_simulator.html#af4658f754d9a3d36dc0b19b0b399c9f9", null ]
];