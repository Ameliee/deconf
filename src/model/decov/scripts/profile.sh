#!/bin/sh
if [ ! -e $1 ]; then
echo "You must pass your executable as a first argument"
exit 1
fi

#./$*
gprof $1 gmon.out > profile.txt
gprof2dot profile.txt > profile.dot
xdot profile.dot
