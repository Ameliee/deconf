#include <string>
#include <iomanip>
namespace decov
{
    template <typename T>
    static std::string to_string(T v)
    {
        std::ostringstream os;
        if constexpr (std::is_floating_point<T>::value)
            os << std::fixed << std::setprecision(2) << v;
        else
            os << v;
        return os.str();
    }

    inline std::string to_lower(std::string s)
    {
        for (char & c: s)
        {
            if (c >= 'A' && c <= 'Z')
                c = c - ('A' - 'a');
        }
        return s;
    }

}
