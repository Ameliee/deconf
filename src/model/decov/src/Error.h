#pragma once
#include <sstream>
#include <exception>
#include <Logger.h>

#define LOCATION decov::join("", __FILE__ ":", __LINE__, ": in ", __func__, ": ")

#ifndef DECOV_ENABLE_ASSERT
#define DECOV_ENABLE_ASSERT 1
#endif
#if DECOV_ENABLE_ASSERT == 1
#define ASSERT(...) do { if (! (__VA_ARGS__)) throw Error("Assertion failed: ", #__VA_ARGS__); } while(false)
#else
#define ASSERT(...)
#endif

namespace decov
{
    struct Error : public std::exception
    {
        Error()
        { }

        template <typename ...Args>
        explicit Error(Args ...args)
        {
            _msg = join("", args...);
        }

        explicit Error(const std::string & s)
        {
           _msg = s;
        }

        const char* what() const throw()
        {
            return _msg.c_str();
        }

        Error(const Error & e)
        {
            _msg = e._msg;
        } 

    protected:
        std::string _msg;
    };
}
