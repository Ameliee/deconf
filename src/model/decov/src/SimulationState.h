/*!
 * \file SimulationState.h
 * \brief File storing agent and spot states during the simulation
 * \author Sam
 * \version 0.1
 */


#pragma once
#include "Random.h"
#include "Spot.h"
#include "Agent.h"
#include "Indicators.h"
#include <vector>
namespace decov
{
/*! \struct SimulationState
 * 
 * Agent and Spot states during simulation
 */
struct SimulationState
{
    std::vector<Spot> spots; /*< list of spots where agent can be*/
    std::vector<Agent> agents; /*< list of agents */

    // Next day to simulate
    int day = 0; /*< current simulation day */

    Indicators indicators; /*< */

    /*! \fn addSpot
     *
     *  add a Spot in the simulation
     * 
     *  \param kind, the spot category
     *  \param size, number of agent that can be in this spot
     *  \return spotid, the id of the spot in the simulation spots vector
     */
    SpotId addSpot(SpotKinds kind, int size)
    {
        spots.emplace_back(kind, size);
        return (SpotId)spots.size() - 1;
    }

    /*! \fn assignAgent
     *
     *  Assign an Agent to a Spot
     * 
     *  \param agentId, the agent to add to a spot
     *  \param spotId, the spot where the agent will be assigned to
     */
    void assignAgent(AgentId agentId, SpotId spotId)
    {
        auto &spot = spots[spotId];
        auto &agent = agents[agentId];

        agent.spotIds[spot.kind] = spotId;
        agent.indexes[spot.kind] = spot.assign(agentId);
    }

    /*! \fn allocateTime
     *
     *  allocate a time to an Agent in a Spot
     * 
     *  \param &agent, the agent 
     *  \param spotKind, the spot category
     *  \param timeRatio, the time allocated for the agent in this spot
     */
    void allocateTime(Agent &agent, SpotKinds spotKind, float timeRatio)
    {
        ASSERT(timeRatio >= 0 && timeRatio <= 1);
        ASSERT(agent.has(spotKind));

        auto spotId = agent.spotIds[spotKind];
        auto index = agent.indexes[spotKind];
        auto &spot = spots[spotId];
        spot.presenceRatios[index] = timeRatio;
    }
};
}
