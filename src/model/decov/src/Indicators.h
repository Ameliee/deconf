#pragma once
namespace decov
{
// Indicators cumulated since the start of the simulation
struct Indicators
{
    EnumArray<int, Stages> counts;

    void changeStage(Stages from, Stages to)
    {
        #pragma omp atomic
        --counts[from];
        
        #pragma omp atomic
        ++counts[to];
    }

    friend std::ostream &operator<<(std::ostream &os, const Indicators &i)
    {
        for (int s = 0; s < (int)Stages::COUNT; ++s)
        {
            auto stage = (Stages)s;
            if (s != 0)
                os << ", ";
            os << stage << "=" << i.counts[stage];
        }
        return os;
    }
};
} // namespace decov
