#pragma once
#include "Definitions.h"
#include "Enum.h"
#include <vector>
namespace decov
{
// Political measures
struct Policy
{
    std::vector<EnumArray<bool, MeasureKinds>> measures;

    // Returns the applied measure for the given day since the start of simulation
    const EnumArray<bool, MeasureKinds> &getMeasures(int day)
    {
        return measures[day];
    }
};
}