/*!
 * \file DataMaker.h
 * \brief File to intialize population data / spots / policies
 * \author Sam
 * \version 0.1
 */

#pragma once
#include "Random.h"
#include "Parameters.h"
#include "SimulationState.h"
#include "Policy.h"

namespace decov
{
class DataMaker
{
    Parameters &parameters;
    SimulationState &state;

    // List of spots currently being assigned:
    EnumArray<std::vector<SpotId>, SpotKinds> freeSpots;
    FastRandom re;

public:
    DataMaker(Parameters &parameters, SimulationState &state)
        : parameters(parameters),
          state(state)
    {
    }

    void makeHolidays()
    {
        parameters.schoolHolidays.resize(parameters.simulationDuration, false);
        
        // Set 7 weeks of holidays after 5 months
        int d = 5*4*7;
        for (int h = 0; h < 7 * 7; ++h)
        {
            if (d == parameters.simulationDuration)
                break;
            parameters.schoolHolidays[d] = true;
            d++;
        }
    }

    // Make a test policy, introducing measure, one after the other, week after week,
    // starting two weeks after the beginning of the simulation
    void makePolicy(Policy &policy)
    {
        policy.measures.resize(parameters.simulationDuration);

        int d = 0;
        // First weeks without measures
        d += 4 * 7;

        // Then, global measures for a few months
        for (int c = 0; c < 10 * 7; ++c)
        {
            if (d == parameters.simulationDuration)
                break;

            policy.measures[d][MeasureKinds::BasicBarrier] = true;
            policy.measures[d][MeasureKinds::CloseSchool] = true;
            policy.measures[d][MeasureKinds::Telework] = true;
            policy.measures[d][MeasureKinds::ConfineAll] = true;
            policy.measures[d][MeasureKinds::ConfineTeleworkOld] = true;
            policy.measures[d][MeasureKinds::ConfineTeleworkSymptomatic] = true;

            d++;
        }
    }

    // Generate a synthetic test population
    void makePopulation(int population, int numLatent)
    {
        initSpotPool();

        const float sum = std::accumulate(parameters.pyramid.begin(), parameters.pyramid.end(), 0);

        for (int p = 0; p < population; ++p)
        {
            AgentId id = (int)state.agents.size();
            state.agents.emplace_back();
            auto &agent = state.agents.back();
            agent.age = (Ages)re.select(parameters.pyramid.data(), 3, sum);

            // Assign to home
            assignToFreeSpot(id, SpotKinds::Home);

            // Assign to work
            if (re.rand() < parameters.EmploymentRates[agent.age])
            {
                assignToFreeSpot(id, SpotKinds::Work);
            }
            // Else to school for young people
            else if (agent.age == Ages::Young)
            {
                assignToFreeSpot(id, SpotKinds::School);
            }

            // Assign to neighborhood spots
            assignToFreeSpot(id, SpotKinds::Neighborhood1);
            assignToFreeSpot(id, SpotKinds::Neighborhood2);
        }

        initStages(numLatent);
    }

private:
    void initSpotPool()
    {
        // Initialize pool of free spots
        for (int k = 0; k < (int)SpotKinds::COUNT; ++k)
        {
            auto spotKind = (SpotKinds)k;
            for (int n = 0; n < parameters.SpotsScatteringFactor[spotKind]; ++n)
            {
                freeSpots[spotKind].push_back(addSpot(spotKind));
            }
        }
    }

    void initStages(int initialLatent)
    {
        ASSERT(initialLatent < (int)state.agents.size());

        std::vector<AgentId> latent;
        for (int i = 0; i < initialLatent; ++i)
        {
            AgentId agentId;
            do
            {
                agentId = re.randInt(0, (int)state.agents.size());
            } while (std::count(latent.begin(), latent.end(), agentId) != 0);
            latent.push_back(agentId);
            state.agents[agentId].stage = Stages::Latent;
        }

        state.indicators.counts[Stages::Sensitive] = (int)state.agents.size() - initialLatent;
        state.indicators.counts[Stages::Latent] = initialLatent;
    }

    // Create a new empty spot, with a random size
    SpotId addSpot(SpotKinds kind)
    {
        int size = re.randInt(parameters.SpotSizes[kind].first,
                              parameters.SpotSizes[kind].second);
        return state.addSpot(kind, size);
    }

    // Assigns at random to a free spot of given kind,
    // and replaces spots as soon as they are full.
    void assignToFreeSpot(AgentId agentId, SpotKinds spotKind)
    {
        auto &spotPool = freeSpots[spotKind];
        int index = 0;
        if (spotPool.size() > 1)
            index = re.randInt(0, (int)spotPool.size() - 1);

        SpotId chosenId = spotPool[index];

        if (state.spots[chosenId].full())
        {
            // Replace by a fresh one
            chosenId = spotPool[index] = addSpot(spotKind);
        }

        state.assignAgent(agentId, chosenId);
    }
};
} // namespace decov