/*!
 * \file Parameters.h
 * \brief File storing agent and spot states during the simulation
 * \author Sam
 * \version 0.1
 */


#pragma once
#include "Definitions.h"
#include <vector>
#include "Enum.h"
namespace decov
{
/*! \struct Parameters
 * 
 * store the parameters of the disease, the agent occupation and interactions, 
 * and the policies
 */
struct Parameters
{
    // To be calibrated to reach the good R0
    float InfectionStrength = 0.6f;

    // Taken from http://alizon.ouvaton.org/Rapport3_Modele.html
    int LatencyDuration = 4; /*< time spend in E state  */
    float LatencyOneDayMoreProbability = .2f; /*< transition from E to A, end of tatency */ // To make 4.2 days
    int AsymptomaticDuration = 1;     /*< time spend asymptomatic */
    int MinorInfectionDuration = 17;  /*< time spend in I1 */
    int MajorInfectionDuration = 20;  /*< time spend in I2 */
    float MajorInfectionRatio = 0.1f; /*< */
    float LethalRate = 0.15f; /*< I2 to D rate */
    float DailyLethalRate = LethalRate / MajorInfectionDuration;

    // Occupation ratio, average on a week
    float workRatio = 0.3f;
    float schoolRatio = 0.3f;
    float socialRatio = 0.2f;
    float confinementNeighborhoodFrequency = 1 / 7.0f;

    // Factor to weight the risks of transmission based on political measures:
    float BasicBarrierPromiscuityFactor = 0.6f;
    float ConfinmnentPromiscuityFactor = 0.4f;

    // Probability for an agent to comply with political measures
    float MeasureComplianceProbability = 0.98f;

    // Maximum number of unique persons met on a unique spot each day
    int MaxContacts = 40;

    // TODO: currently, we use uniform distribution for spot size
    const EnumArray<std::pair<int, int>, SpotKinds> SpotSizes = {{
        {{1, 6},      // Home
         {1, 10000},  // Work
         {200, 2000}, // School
         {10, 1000},  // Neighborhood1
         {10, 1000}}  // Neighborhood2
    }};

    // Ratio of working people by age
    const EnumArray<float, Ages> EmploymentRates = {{{0.01f, 0.90f, 0.05f}}};

    // Repartition of population by age
    // Age pyramid 2020, INSEE:
    std::array<float, 3> pyramid = {15.39f, 36.05f, 13.45f};

    const EnumArray<int, SpotKinds> SpotsScatteringFactor = {{{
        1,    // Home
        1000, // Work
        6,    // School
        10,   // Neighborhood1
        10    // Neighborhood2
    }}};

    // Nunber of simulated days
    int simulationDuration = 100;

    std::vector<bool> schoolHolidays;

    Parameters()
        : schoolHolidays(simulationDuration)
    {
    }
};
} // namespace decov