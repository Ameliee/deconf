#pragma once
#include "Enum.h"
namespace decov
{
ENUM(Stages, Sensitive, Latent, Asymptomatic, InfectedMinor, InfectedMajor, Recovered, Dead)

ENUM(SpotKinds, Home, Work, School, Neighborhood1, Neighborhood2)

ENUM(MeasureKinds, BasicBarrier, ConfineTeleworkSymptomatic, CloseSchool, Telework, ConfineTeleworkOld, ConfineAll)

ENUM(Ages, Young, Adult, Aged)

// Index in the global spots list
typedef int SpotId;

// Index in the gloal agent list
typedef int AgentId;

// Index in the spot daily presence list
typedef int IndexInSpot;
}