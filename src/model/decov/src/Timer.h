#include <chrono>
namespace decov
{
struct Timer
{
    std::chrono::high_resolution_clock::time_point start_time;

    void start()
    {
        start_time = std::chrono::high_resolution_clock::now();
    }

    // Returns number of seconds elapsed since last start() call.
    double elapsed()
    {
        auto stop_time = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time).count() / 1000.0;
    }
};
}
