#pragma once
#include "Definitions.h"
#include <vector>
namespace decov
{
struct Spot
{
    const SpotKinds kind;
    const int size;

    // Agents assigned to this spot
    std::vector<AgentId> agents;

    // Presence ratio for current day, for each assigned agent
    std::vector<float> presenceRatios;

    Spot(SpotKinds kind, int size)
        : kind(kind),
          size(size)
    {
    }

    bool full() const
    {
        return (int)agents.size() == size;
    }

    IndexInSpot assign(AgentId agentId)
    {
        agents.push_back(agentId);
        presenceRatios.push_back(0);
        return (IndexInSpot)agents.size() - 1;
    }

    friend std::ostream & operator << (std::ostream & os, const Spot & spot)
    {
        os << spot.kind << " " << spot.agents.size() << "/" << spot.size << " agents, total time=" 
           << std::accumulate(spot.presenceRatios.begin(), spot.presenceRatios.end(), 0.0f);
           return os;
    }
};
}