/*!
 * \file Enum.h
 * \brief File containing template for the different enum
 * \author Sam
 * \version 0.1
 */


#pragma once
#include <regex>
#include <vector>
#include <string>
#include "Format.h"
#include "Error.h"

#define ENUM(NAME, ...)                                           \
enum class NAME                                                   \
{                                                                 \
    __VA_ARGS__,                                                  \
    COUNT                                                         \
};                                                                \
template<>                                                        \
inline const std::vector<std::string> & get_enum_names<NAME>()    \
{                                                                 \
    static auto names = parse_enum_list(","#__VA_ARGS__);         \
    return names;                                                 \
}                                                                 \
template<>                                                        \
inline const char * get_enum_name<NAME>()                         \
{                                                                 \
    return #NAME;                                                 \
}                                                                 \
inline std::ostream & operator << (std::ostream & os, NAME value) \
{                                                                 \
    os << get_enum_name<NAME>(value);                             \
    return os;                                                    \
}

namespace decov
{
    // Array indexed by an enum
    template <typename T, typename Enum>
    struct EnumArray
    {
        void clear(const T & t = T())
        {
            _array.fill(t);
        }

        T & operator [] (Enum e)
        {
            return _array[(int)e];
        }

        const T & operator [] (Enum e) const
        {
            return _array[(int)e];
        }

        std::array<T, (int)Enum::COUNT> _array = {};
    };

    // Specialized by ENUM macro
    template <typename Enum>
    const std::vector<std::string> & get_enum_names();

    // Specialized by ENUM macro
    template <typename Enum>
    const char * get_enum_name();

    template <typename Enum>
    constexpr std::array<Enum, (int)Enum::COUNT> get_enum_values()
    {
        std::array<Enum, (int)Enum::COUNT> values;
        for (int i = 0; i < (int) Enum::COUNT; ++i)
        {
            values[i] = static_cast<Enum>(i);
        }
        return values;
    }

    template <typename Enum>
    const std::string & get_enum_name(Enum value)
    {
        int v = (int)value;
        if (v < 0 || v >= (int)Enum::COUNT)
            throw Error(LOCATION, "Invalid value ", v, " for enum ", get_enum_name<Enum>());
        return get_enum_names<Enum>()[v];
    }

    template <typename Enum>
    bool try_parse_enum(const std::string & name, Enum & value)
    {
        auto & names = get_enum_names<Enum>();
        for (int i = 0; i < (int)names.size(); ++i)
        {
            if (names[i] == name)
            {
                value = (Enum)i;
                return true;
            }
        }
        return false;
    }

    // For use from macro below
    inline std::vector<std::string> parse_enum_list(const std::string & s)
    {
        std::vector<std::string> list;
        std::regex re(",\\s*([A-Z][A-Za-z]*)");
        for (auto it = std::sregex_iterator(s.begin(), s.end(), re);
             it != std::sregex_iterator();
             ++it)
        {
            auto match = *it;
            list.push_back(to_lower(match[1].str()));
        }
        return list;
    }
}
