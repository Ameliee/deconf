/*!
 * \file Simulation.h
 * \brief Main file regarding the multi agent simulation (MAS)
 * \author Sam
 * \version 0.1
 */


#pragma once
#include "Simulator.h"
#include "Timer.h"
#include "Logger.h"
#include "DataMaker.h"
#include <algorithm>
#include <fstream>

/*! \namespace decov
 * 
 * the principal name space with all tools associated to
 * the MAS
 */
namespace decov
{
/*! \struct Simulation
 * 
 * structure of the simulation holding all the parameters
 */
struct Simulation
{
    Parameters parameters;  /*!< structure of simulation parameters*/
    SimulationState state;  /*!< structure of simulation state*/
    Policy policy;          /*!< structure of simulation policies*/
    Timer timer;            /*!< general timer*/
    std::ofstream report;   /*!< output file*/


    Simulation(int population, int initialLatent, int duration, const std::string &reportPath)
        : report(reportPath)
    {
        std::cout << "Initialize population of " << population << std::endl;

        timer.start();

        DataMaker dataMaker(parameters, state);

        // Populate parameters
        parameters.simulationDuration = duration;
        dataMaker.makeHolidays();

        // Initialize populations and spots
        dataMaker.makePopulation(population, initialLatent);

        // Create a test policy
        dataMaker.makePolicy(policy);

        // Compute and print stats
        int room = 0;
        for (auto &spot : state.spots)
        {
            room += spot.size;
        }

        std::cout << "Total spots: " << state.spots.size() << std::endl
                  << "Total spot room: " << room << std::endl
                  << "Average spot size: " << ((double)room / state.spots.size()) << std::endl
                  << "Average spot room per agent: " << (double)room / population << std::endl
                  << "Initialization time: " << timer.elapsed() << std::endl;
    }

    void writeReportHeader()
    {
        report << "day,simulation_time";
        for (int s = 0; s < (int)Stages::COUNT; ++s)
        {
            report << "," << (Stages)s;
        }
        for (int p = 0; p < (int)MeasureKinds::COUNT; ++p)
        {
            report << "," << (MeasureKinds)p;
        }
        report << ",holidays\n";
        report.flush();
    }

    void writeReportLine()
    {
        report << state.day << "," << timer.elapsed();
        for (int s = 0; s < (int)Stages::COUNT; ++s)
        {
            auto stage = (Stages)s;
            report << "," << state.indicators.counts[stage];
        }
        for (int p = 0; p < (int)MeasureKinds::COUNT; ++p)
        {
            if (state.day == 0)
                report << ",-";
            else
                report << "," << policy.getMeasures(state.day - 1)[(MeasureKinds)p];
        }
        if (state.day == 0)
            report << ",-";
        else
            report << "," << parameters.schoolHolidays[state.day - 1];
        report << "\n";
        report.flush();
    }

    void run()
    {
        Simulator simulator(parameters, state, policy);

        std::cout << "Run simulation for " << parameters.simulationDuration << " days" << std::endl;
        std::cout << "Initial situation:" << std::endl;
        std::cout << state.indicators << std::endl;
        writeReportHeader();
        writeReportLine();

        timer.start();
        while (state.day < parameters.simulationDuration)
        {
            simulator.runDay();
            std::cout << "After day " << state.day << ":" << std::endl;
            std::cout << state.indicators << std::endl;
            std::cout << "Simulation time: " << timer.elapsed() << std::endl
                      << std::endl;
            writeReportLine();
        }
    }
};
} // namespace decov
