#pragma once
#include "Parameters.h"
#include "SimulationState.h"
#include "Policy.h"
namespace decov
{
struct Simulator
{
    Parameters &parameters;
    SimulationState &state;
    Policy &policy;

    static FastRandom re;

    // Used to randomize agent contacts
    static std::vector<IndexInSpot> choiceBuffer;

    #pragma omp threadprivate(re, choiceBuffer)

    Simulator(Parameters &parameters, SimulationState &state, Policy &policy)
        : parameters(parameters),
          state(state),
          policy(policy)
    {
        ENABLE_TRACE_FOR(this);
    }

    void runDay()
    {
        allocateTime();
        spreadInfection();
        updateAgentsStages();
        state.day++;
    }

    // Agents choose their time spent in each assigned spot according to the policy
    void allocateTime()
    {
        auto &measures = policy.getMeasures(state.day);

        #pragma omp parallel for
        for (int a = 0; a < state.agents.size(); ++a)
        {
            auto &agent = state.agents[a];

            float homeTime = 1;
            float workTime = 0;
            float schoolTime = 0;
            float neighborhood1Time = 0;
            float neighborhood2Time = 0;

            switch (agent.stage)
            {
            case Stages::Sensitive:
            case Stages::Latent:
            case Stages::Asymptomatic:
            case Stages::Recovered:
            case Stages::InfectedMinor:
            {
                bool infringe = re.rand() > parameters.MeasureComplianceProbability;
                // TODO: chech holidays, how to simulate holidays for workers?
                bool work = agent.has(SpotKinds::Work) && (infringe || !measures[MeasureKinds::Telework]);
                bool school = agent.has(SpotKinds::School) &&
                              (infringe || !measures[MeasureKinds::CloseSchool]) &&
                              !parameters.schoolHolidays[state.day];
                bool socialize = infringe || !measures[MeasureKinds::ConfineAll];

                if (!infringe)
                {
                    if ((agent.stage == Stages::InfectedMinor && measures[MeasureKinds::BasicBarrier]) ||
                        (agent.age == Ages::Aged && measures[MeasureKinds::ConfineTeleworkOld]))
                    {
                        work = school = socialize = false;
                    }
                }

                if (work)
                {
                    workTime = parameters.workRatio;
                    homeTime -= parameters.workRatio;
                }
                else if (school)
                {
                    schoolTime = parameters.schoolRatio;
                    homeTime -= parameters.schoolRatio;
                }

                if (socialize || re.rand() < parameters.confinementNeighborhoodFrequency)
                {
                    if (re.rand() < 0.5)
                        neighborhood1Time = parameters.socialRatio;
                    else
                        neighborhood2Time = parameters.socialRatio;
                    homeTime -= parameters.socialRatio;
                }
            }
            break;
            case Stages::InfectedMajor:
            case Stages::Dead:
                homeTime = 0;
                break;
            case Stages::COUNT:
                throw; // Cannot happen
            }

            state.allocateTime(agent, SpotKinds::Home, homeTime);
            if (agent.has(SpotKinds::Work))
                state.allocateTime(agent, SpotKinds::Work, workTime);
            else if (agent.has(SpotKinds::School))
                state.allocateTime(agent, SpotKinds::School, schoolTime);
            state.allocateTime(agent, SpotKinds::Neighborhood1, neighborhood1Time);
            state.allocateTime(agent, SpotKinds::Neighborhood2, neighborhood2Time);

            TRACE("Allocate:");
            TRACE(" home=", homeTime);
            TRACE(" work=", workTime);
            TRACE(" school=", schoolTime);
            TRACE(" neighborhood1=", neighborhood1Time);
            TRACE(" neighborhood2=", neighborhood2Time);
        }
    }

    // Computes who is newly infected after this day
    void spreadInfection()
    {
        auto &measures = policy.getMeasures(state.day);

        float promiscuityFactor;
        if (!measures[MeasureKinds::BasicBarrier])
            promiscuityFactor = 1;
        else if (measures[MeasureKinds::ConfineAll])
            promiscuityFactor = parameters.ConfinmnentPromiscuityFactor;
        else
            promiscuityFactor = parameters.BasicBarrierPromiscuityFactor;

        // For each sensitive agent in each spot, compute the infected flag according to met agents.
        #pragma omp parallel for
        for (int s = 0; s < state.spots.size(); ++s)
        {
            auto &spot = state.spots[s];

            // Fill buffer of agents that spent time in the this spot
            choiceBuffer.clear();
            bool hasContagious = false;
            bool hasSensitive = false;
            for (int i = 0; i < spot.agents.size(); ++i)
            {
                if (spot.presenceRatios[i] > 0)
                {
                    choiceBuffer.push_back(i);
                    auto& agent = state.agents[spot.agents[i]];
                    hasContagious |= agent.contagious();
                    hasSensitive |= (agent.stage == Stages::Sensitive);
                }
            }
            TRACE(choiceBuffer.size(), "present in spot", spot.kind, "of size", spot.size);
            if ((! hasContagious) | (! hasSensitive) | (choiceBuffer.size() <= 1))
                continue;
            int maxContacts;
            if (choiceBuffer.size() > parameters.MaxContacts + 1)
            {
                maxContacts = parameters.MaxContacts;
                re.shuffle(choiceBuffer.begin(), choiceBuffer.end());
            }
            else
                maxContacts = (int)choiceBuffer.size() - 1;

            for (int a: choiceBuffer)
            {
                AgentId agentId = spot.agents[a];
                auto &agent = state.agents[agentId];
                if (agent.stage != Stages::Sensitive)
                    continue;
                int c = re.randInt(0, (int)choiceBuffer.size() - 1);

                for (int contacts = 0; contacts < maxContacts; ++contacts)
                {
                    IndexInSpot chosen = choiceBuffer[c];
                    c = (c + 1) % (int)choiceBuffer.size();
                    if (chosen == a)
                    {
                        --contacts;
                        continue;
                    }
                   
                    if (simulateContact(spot, agent, a, chosen, promiscuityFactor))
                        break;
                }
            }
        }
    }

    // Returns true if the sensitive agent was infected
    bool simulateContact(const Spot &spot, Agent &sensitiveAgent, IndexInSpot sensitiveAgentIndex, IndexInSpot otherAgentIndex, float promiscuityFactor)
    {
        TRACE("Simulate contact at", spot, "with factor", promiscuityFactor);
        AgentId otherId = spot.agents[otherAgentIndex];
        auto &other = state.agents[otherId];
        
        if (other.contagious())
        {
            float e = re.rand();
            float p = parameters.InfectionStrength *
                      spot.presenceRatios[sensitiveAgentIndex] *
                      spot.presenceRatios[otherAgentIndex] *
                      promiscuityFactor;
            if (e < p)
            {
                TRACE("Infect agent");
                sensitiveAgent.infected = true;
                return true;
            }
        }
        
        return false;
    }

    // Update infection stages
    void updateAgentsStages()
    {
        #pragma omp parallel for
        for (int a = 0; a < (int)state.agents.size(); ++a)
        {
            auto &agent = state.agents[a];
            int elapsedDays = state.day - agent.stageBeginDay;
            switch (agent.stage)
            {
            case Stages::Sensitive:
                if (agent.infected)
                {
                    state.indicators.changeStage(Stages::Sensitive, Stages::Latent);
                    agent.stage = Stages::Latent;
                    agent.stageBeginDay = state.day;
                }
                break;
            case Stages::Latent:
                if (elapsedDays > parameters.LatencyDuration ||
                    (elapsedDays == parameters.LatencyDuration &&
                     re.rand() > parameters.LatencyOneDayMoreProbability))
                {
                    state.indicators.changeStage(Stages::Latent, Stages::Asymptomatic);
                    agent.stage = Stages::Asymptomatic;
                    agent.stageBeginDay = state.day;
                }
                break;

            case Stages::Asymptomatic:
                if (elapsedDays == parameters.AsymptomaticDuration)
                {
                    if (re.rand() < parameters.MajorInfectionRatio)
                    {
                        state.indicators.changeStage(Stages::Asymptomatic, Stages::InfectedMajor);
                        agent.stage = Stages::InfectedMajor;
                    }
                    else
                    {
                        state.indicators.changeStage(Stages::Asymptomatic, Stages::InfectedMinor);
                        agent.stage = Stages::InfectedMinor;
                    }
                    agent.stageBeginDay = state.day;
                }
                break;
            case Stages::InfectedMinor:
                if (elapsedDays == parameters.MinorInfectionDuration)
                {
                    state.indicators.changeStage(Stages::InfectedMinor, Stages::Recovered);
                    agent.stage = Stages::Recovered;
                    agent.stageBeginDay = state.day;
                }
                break;
            case Stages::InfectedMajor:
                if (re.rand() < parameters.DailyLethalRate)
                {
                    state.indicators.changeStage(Stages::InfectedMajor, Stages::Dead);
                    agent.stage = Stages::Dead;
                }
                else if (elapsedDays == parameters.MajorInfectionDuration)
                {
                    state.indicators.changeStage(Stages::InfectedMajor, Stages::Recovered);
                    agent.stage = Stages::Recovered;
                    agent.stageBeginDay = state.day;
                }
                break;
            case Stages::Recovered:
            case Stages::Dead:
                break;
            case Stages::COUNT:
                throw; // Cannot happen
            }
        }
    }
};
} // namespace decov
