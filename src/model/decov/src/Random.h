#pragma once
#include <random>
#include "Error.h"
namespace decov
{
// Xorshf96 period 2^96-1
struct FastRandom
{
    unsigned long x = 123456789, y = 362436069, z = 521288629;

    unsigned long randULong()
    {
        unsigned long t;
        x ^= x << 16;
        x ^= x >> 5;
        x ^= x << 1;

        t = x;
        x = y;
        y = z;
        z = t ^ x ^ y;

        return z;
    }

    // Returns int in range [min;max]
    unsigned long randULong(unsigned long min, unsigned long max)
    {
        ASSERT(min < max);
        return min + randULong() % (max - min);
    }

    // Returns int in range [min;max]
    int randInt(int min, int max)
    {
        ASSERT(min < max);
        return min + (int)(randULong() % (max - min));
    }

    // Returns in range [0;1)
    float rand()
    {
        static constexpr unsigned long max = 1000000;
        return (float)(randULong() % max) / max;
    }

    template <class RandomIt>
    void shuffle(RandomIt first, RandomIt last)
    {
        typename std::iterator_traits<RandomIt>::difference_type i, n;
        n = last - first;
        for (i = n - 1; i > 0; --i)
        {
            std::swap(first[i], first[randULong() % (i + 1)]);
        }
    }

    // Roulette wheel
    int select(const float *array, int count, float sum)
    {
        ASSERT(sum > 0);
        float acc = 0;
        float p = rand() * sum;
        for (int i = 0; i < count; ++i)
        {
            float weight = array[i];
            if (weight <= 0)
                continue; 
            acc += weight;
            if (p <= acc)
                return i;
        }
        throw;
    }
};

struct Random
{
    // Best quality/perf ratio for clang-9:
    std::mt19937_64 _random;

    Random()
        : _random()
    {
    }

    int randInt(int min, int max)
    {
        return std::uniform_int_distribution<int>(min, max)(_random);
    }

    // Returns inside [0; 1)
    float rand()
    {
        return std::uniform_real_distribution<float>(0, 1)(_random);
    }

    template <typename IT>
    void shuffle(IT begin, IT end)
    {
        std::shuffle(begin, end, _random);
    }

    // Roulette wheel
    int select(const float *array, int count, float sum)
    {
        ASSERT(sum > 0);
        float acc = 0;
        float p = rand() * sum;
        for (int i = 0; i < count; ++i)
        {
            float weight = array[i];
            if (weight <= 0)
                continue;
            acc += weight;
            if (p <= acc)
                return i;
        }
        throw;
    }
};
} // namespace decov
