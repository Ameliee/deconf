# define _DECOV_CONCAT_LAZY(x, y) x ## y
# define DECOV_CONCAT(x, y) _DECOV_CONCAT_LAZY(x, y)

# define _DECOV_STR_LAZY(x) #x
# define DECOV_STR(x) _DECOV_STR_LAZY(x)

# define DECOV_EVAL_MACRO(MACRO, ...) MACRO(__VA_ARGS__)
