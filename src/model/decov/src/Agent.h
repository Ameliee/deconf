#pragma once
#include "Definitions.h"
namespace decov
{
struct Agent
{
    Stages stage = Stages::Sensitive;
    int stageBeginDay = 0;
    Ages age = Ages::Adult;

    // Assigned spots
    EnumArray<SpotId, SpotKinds> spotIds;
    EnumArray<IndexInSpot, SpotKinds> indexes;

    bool infected = false; //< Records if this agent must pass from Sensitive to Latent during this day

    Agent()
    {
        spotIds.clear(-1);
        indexes.clear(-1);
    }

    bool has(SpotKinds spotKind) const
    {
        return spotIds[spotKind] != -1;
    }

    bool contagious() const
    {
        return (stage == Stages::Asymptomatic) |
               (stage == Stages::InfectedMinor) |
               (stage == Stages::InfectedMajor);
    }

    friend std::ostream & operator << (std::ostream & os, const Agent & agent)
    {
        os << agent.age << " " << agent.stage << " since " << agent.stageBeginDay;
        return os;
    }
};
}