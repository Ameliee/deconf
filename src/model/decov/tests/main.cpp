
#include <map>
#include <Error.h>
#include <iostream>

using namespace decov;

template <typename Test>
void run(std::map<std::string, int> &testNames, bool dontCatch, bool display)
{
    auto name = type_name<Test>();
    name.remove_prefix(8);
    if (!testNames.empty())
    {
        bool found = false;
        for (auto &[pattern, count] : testNames)
        {
            if (name.find(pattern) != name.npos)
            {
                std::cout << name << " matches " << pattern << "\n";
                count++;
                found = true;
            }
        }
        if (!found)
            return;
    }

    std::cout << name << " ";
    if (dontCatch)
    {
        Test(display).run();
    }
    else
    {
        try
        {
            Test(display).run();
        }
        catch (...)
        {
            std::cout << "\33[91mFAILED\33[0m\n";
            return;
        }
    }
    std::cout << "\33[32mPASSED\33[0m\n";
}



int main(int argc, const char **argv)
{
    std::map<std::string, int> testsToRun;
    bool dontCatch = false;
    bool display = false;
    for (int a = 1; a < argc; ++a)
    {
        std::string arg = argv[a];
        if (arg == "-d")
            dontCatch = true;
        else if (arg == "-i")
            display = true;
        else if (arg[0] != '-')
            testsToRun.emplace(arg, 0);
        else
            throw Error("Unknown option:", arg);
    }

#define T(NAME) run<NAME##Test>(testsToRun, dontCatch, display);

#undef T

    for (auto &[name, count] : testsToRun)
    {
        if (count == 0)
            throw Error("No test matching: ", name);
    }
    return 0;
}
