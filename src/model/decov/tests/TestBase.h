#pragma once
#include <cmath>
#include "Error.h"

struct TestBase
{
    TestBase()
    {
        //ENABLE_TRACE_FOR(this);

        reset();
    }

    virtual ~TestBase()
    {

    }

    virtual void reset()
    {
       
    }

    template <typename... Args>
    void check(bool condition, Args... msg)
    {
        if (!condition)
            throw Error(msg...);
    }

    template <typename... Args>
    void checkAlmostEqual(float v1, float v2, Args... msg)
    {
        if (std::abs(v1 - v2) > 0.005f)
            throw Error(v1, "!=", v2, ": ", msg...);
    }

    virtual void run() = 0;
};
