# encoding=utf8
import csv
import json
import pathlib
import sys

path_to_root = str(pathlib.Path(__file__).parent.parent.parent.absolute())


class Economie:
    def __init__(self):
        with open(path_to_root + '/data/data_impact_branches.csv') as fp:
            reader = csv.DictReader(fp)
            self.impact = dict()
            for row in reader:
                key = row["branches"]
                del row["branches"]
                self.impact[key] = {k: float(a) for k, a in row.items()}

        with open(path_to_root + '/data/data_emploi_region.json') as fp:
            self.emploi = json.load(fp)

        emploi_national_branches = {branch: 0 for branch in self.impact}
        for reg in self.emploi:
            for branch in self.emploi[reg]:
                emploi_national_branches[branch] += self.emploi[reg][branch]

        self.part_pib_region = dict()
        for reg in self.emploi:
            self.part_pib_region[reg] = dict()
            for branch in self.impact:
                self.part_pib_region[reg][branch] = self.impact[branch]['part'] * self.emploi[reg][branch] / \
                                                    emploi_national_branches[branch]

    def evaluate(self, req, weeks):
        if isinstance(req, str):
            with open(req) as fp:
                req = json.load(fp)
        zones = req['zones']
        gdb_accumulated_loss = [0]
        gdb_loss = 0
        for week in range(weeks):
            if week < req['measure_weeks'][0]:
                last_week = 0
            else:
                last_week = [j for j, i in enumerate(req['measure_weeks']) if i <= week][-1]

            for j, reg in enumerate(zones):
                for branch in self.impact:
                    gdb_branch_reg_ecoles = self.part_pib_region[reg][branch] * \
                                            self.impact[branch]['ecoles']
                    gdb_branch_reg_confinement = self.part_pib_region[reg][branch] * \
                                                 self.impact[branch]['confinement']
                    gdb_branch_reg_teletravail = self.part_pib_region[reg][branch] * \
                                                 self.impact[branch]['teletravail']

                    school_closed = True if last_week == 0 else req["school_closures"] is not None and \
                                                                req["school_closures"][last_week]['activated'][j]
                    gdb_loss += school_closed * gdb_branch_reg_ecoles / 52
                    socially_contained = True if last_week == 0 else req["social_containment_all"] is not None and \
                                                                     req["social_containment_all"][last_week][
                                                                         'activated'][j]
                    gdb_loss += socially_contained * gdb_branch_reg_confinement / 52
                    telecommuting = True if last_week == 0 else req["forced_telecommuting"] is not None and \
                                                                req["forced_telecommuting"][last_week]['activated'][j]
                    gdb_loss += telecommuting * gdb_branch_reg_teletravail / 52
                    # TODO : confinement des personnes actives à risque, comme un pourcentage (5%?) de télétravail+social_containment, mais sans doublon
                    gdb_loss += 0

            gdb_accumulated_loss.append(gdb_loss)

        return gdb_accumulated_loss


# test
if __name__ == '__main__':
    if len(sys.argv) == 1:
        filename = path_to_root + "/doc/eval_policy_req.json"
    else:
        filename = sys.argv[1]

    economie = Economie()
    print(economie.evaluate(filename, 10))
