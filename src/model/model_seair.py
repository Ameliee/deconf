# -*- coding: utf-8 -*-
from copy import deepcopy

import numpy as np
from scipy.integrate import odeint


def mu_func(x1, x2, x3):
    """ fonction de mortalité liée à la saturation des hôpitaux
    """
    #  (1 - x1) * 0.001 / (1 + exp(-0.05 * (x2 - i.sat))) # 0 morts tant que l'hosto n'es pas saturé.
    return x1 * (x2 > x3)


def covid_ode(y, t, params):
    # fonction pour le système d'ODE lui-même
    # S=y[1] ; E1=y[2] ; A1=y[3] ; I1=y[4] ; R1=y[5] ;
    # E2=y[6] ; A2=y[7] ; I2=y[8] ; R2=y[9] ; M=y[10]

    # Lambda <- (1 - c) * Beta * (y[3] + y[7] + y[4] + bb * y[8])
    Lambda = (1 - params["c"]) * params["beta"] * (y[2] + y[6] + y[3] + params["bb"] * y[7])
    # Mu <- Mu_func(mort_sup, y[8],i.sat)
    Mu = mu_func(params["mort_sup"], y[7], params["i_sat"])
    # Alpha <- Virulence + Mu_func(vir_sup, y[8],i.sat)
    Alpha = params["virulence"] + mu_func(params["vir_sup"], y[7], params["i_sat"])

    N_tot = sum(y[-10:])  # ?
    # dS <- - Lambda * y[1] - Mu * y[1]
    dS = - Lambda * y[0] - Mu * y[0]
    # dE1 <- Mild * Lambda * y[1] - Epsilon * y[2] + Mild * Nu - Mu * y[2] 
    dE1 = params["mild"] * Lambda * y[0] - params["epsilon"] * y[1] + params["mild"] * params["nu"] - Mu * y[1]
    # dA1 <- Epsilon * y[2] - Sigma * y[3] - Mu * y[3]
    dA1 = params["epsilon"] * y[1] - params["sigma"] * y[2] - Mu * y[2]
    # dI1 <- Sigma * y[3] - (Gamm1 + Mu) * y[4]
    dI1 = params["sigma"] * y[2] - (params["gamm1"] + Mu) * y[3]
    # dR1 <- Gamm1 * y[4] - Mu * y[5]
    dR1 = params["gamm1"] * y[3] - Mu * y[4]
    # dE2 <- (1 - Mild) * Lambda * y[1] - Epsilon * y[6] + (1 - Mild) * Nu - Mu * y[6]
    dE2 = (1 - params["mild"]) * Lambda * y[0] - params["epsilon"] * y[5] + (1 - params["mild"]) * params["nu"] - Mu * \
          y[5]
    # dA2 <- Epsilon * y[6] - Sigma * y[7] - Mu * y[7]
    dA2 = params["epsilon"] * y[5] - params["sigma"] * y[6] - Mu * y[6]
    # dI2 <- Sigma * y[7] - (Gamm2 + Mu + Alpha) * y[8]
    dI2 = params["sigma"] * y[6] - (params["gamm2"] + Mu + Alpha) * y[7]
    # dR2 <- Gamm2 * y[8] - Mu * y[9]
    dR2 = params["gamm2"] * y[7] - Mu * y[8]
    # dM <- Alpha * y[8] + Mu * (N_tot-y[8])
    dM = Alpha * y[7] + Mu * (N_tot - y[7])

    return [dS, dE1, dA1, dI1, dR1, dE2, dA2, dI2, dR2, dM]


def default_parameters():
    """ default parameters for Alizon's model 
    """
    # default parameters
    parameters = {
        "epsilon": 1.0,  # taux de fin de latence, jours-1 TODO Arnaud 1/4.2 chez Alizon
        "sigma": 0.4,  # taux d’apparition des symptomes, jour-1 TODO Arnaud 1 chez Alizon
        "R0": 2.5,  # taux de reproduction initial
        "N0": 1,  # nb total de personnes dans la population initiale
        "I0": 0.000,  # nb de personnes infectées dans la population initiale
        "gamm1": 0.06,  # taux de guérison des infections légères, jours-1
        "gamm2": 0.05,  # taux de guérison des infections lourdes, jours-1
        "CFR": 0.15,  # case fatality rate = taux de létalité des infections hospitalisées
        "nu": 10 ** -6,  # Nu (migration), jour−1
        "mild": 0.9,  # Mild (proportion = p), proportion des infections qui ne nécessitent pas l’hospitalisation
        "bb": 0.2,  # a decrease in transmission in hospitals
        "mort_sup": 0.00001,  # mortality due to hospital saturation
        "vir_sup": 0.01,  # virulence due to hospital saturation
        "c": [0.0],  # control of the virus, one per each intervals
        "i_sat": 2 * 10 ** -1,  # satutation du système hospitalier
        "time_interval": [0, 365 * 2],  # jours, number of intervals = len("time_interval") - 1, first value unused
        # y0: [0]=S [1]=E1 [2]=A1 [3]=var1 (I1=var1*I0) [4]=R1 [5]=E2 [6]=A2 [7]=var2 (I2=var2*I0) [8]=R2 [9]=M
        # y0 is None because it will be computed later using N0 and the other SEAIR quantities
        "y0": [None, 0, 0, 0.8, 0, 0, 0, 0.2, 0, 0],
    }
    return parameters


def SEAIR2params(S: float, E1: float, E2: float, A1: float, A2: float, I1: float, I2: float, R1: float, R2: float,
                 M: float, params: dict):
    I0 = I1 + I2
    total = S + E1 + E2 + A1 + A2 + I1 + I2 + R1 + R2 + M
    mild = I1 / (I1 + I2)
    params['y0'] = [S, E1, A1, mild, R1, E2, A2, 1 - mild, R2, M]
    params['N0'] = total
    params['I0'] = I0
    return True


def measures2c(forced_telecommuting: False,
               school_closures: False,
               social_containment_all: False,
               total_containment_high_risk: False):
    c = 0
    # TODO Arnaud: to check by TBF, random values
    if forced_telecommuting:
        c = c + .3
    if social_containment_all:
        c = c + .2
    if school_closures:
        c = c + .3
    if total_containment_high_risk:
        c = c + .1
    return c


def SEAIR_ratios2params(npop: int, S: float, E1: float, E2: float, A1: float, A2: float, I1: float, I2: float,
                        R1: float, R2: float, M: float, params: dict):
    if S + E1 + E2 + A1 + A2 + I1 + I2 + R1 + R2 + M - 1 > 1E-4:
        print('ratio sum (should be 1):', S + E1 + E2 + A1 + A2 + I1 + I2 + R1 + R2 + M)
        return False
    r = npop / (S + E1 + E2 + A1 + A2 + I1 + I2 + R1 + R2 + M)  # ratio
    return SEAIR2params(S * r, E1 * r, E2 * r, A1 * r, A2 * r, I1 * r, I2 * r, R1 * r, R2 * r, M * r, params)


class SEAIRModel:
    """ The SEAIR model from http://alizon.ouvaton.org/Report3_Model.html
    """

    def __init__(self, alizon20_parameters: dict):
        assert len(alizon20_parameters["time_interval"]) - 1 == len(alizon20_parameters["c"])
        self.parameters = deepcopy(alizon20_parameters)

        # TODO put initial conditions in parameters ?
        # Initial conditions
        # self.N0 = 1 # normalise la taille de population à 1
        # self.I0 = .000 ##initiation via migration
        # self.S0 = self.N0 - self.I0
        self.N0 = self.parameters["N0"]
        self.I0 = self.parameters["I0"]
        self.S0 = self.N0 - self.I0

        # adjust y0
        self.parameters["y0"][0] = self.S0
        self.parameters["y0"][3] *= self.I0  # I1 = var1*I0
        self.parameters["y0"][7] *= self.I0  # I2 = var2*I0

        # calculate virulence from case fatality ratio
        self.virulence = self.parameters["CFR"] * (self.parameters["gamm2"]) / (1 - self.parameters["CFR"])

        # calculer Beta depuis R0
        self.Beta = self.parameters["R0"] * self.parameters["gamm1"] * self.parameters["sigma"] * (
                self.virulence + self.parameters["gamm2"])
        denom = self.virulence * self.parameters["gamm1"] + self.parameters["gamm1"] * self.parameters["gamm2"]
        denom += self.parameters["bb"] * self.parameters["gamm1"] * self.parameters["sigma"]
        denom += self.virulence * self.parameters["mild"] * self.parameters["sigma"]
        denom -= self.parameters["bb"] * self.parameters["gamm1"] * self.parameters["mild"] * self.parameters["sigma"]
        denom += self.parameters["gamm2"] * self.parameters["mild"] * self.parameters["sigma"]
        self.Beta /= (self.S0 * denom)

    def compute(self, c=None, time_interval=None, rtol=1e-6, atol=1e-6):
        """ compute the seair model given parameters
        :return: n days x [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
        """
        results = []
        # parameters used in the function covid_ode passed in a dictionary
        covid_parameters_names = ["epsilon", "sigma", "gamm1", "gamm2",
                                  "mild", "nu", "bb", "c", "mort_sup", "vir_sup", "i_sat"]
        myc = c if c is not None else self.parameters["c"]
        myt = time_interval if time_interval is not None else self.parameters["time_interval"]
        
        for i in range(len(myt) - 1):
            # ode solver extra parameters dictionary
            params = dict((key, self.parameters[key]) for key in covid_parameters_names)
            params["c"] = myc[i]
            params["beta"] = self.Beta
            params["virulence"] = self.virulence

            beg = myt[i]
            end = myt[i + 1] + 1

            # Resolution ODEs sans contrôle
            if len(results) == 0:
                y0 = self.parameters["y0"]  # [self.S0, 0, 0, 0.8 * self.I0, 0, 0, 0, 0.2 * self.I0, 0, 0]
            else:
                beg += 1
                y0 = results_interv[-1]

            results_interv = odeint(covid_ode, y0=y0, t=range(beg, end), args=(params,),
                                    rtol=rtol, atol=atol, mxstep=5000)
            results.append(results_interv)

        results = np.vstack(results)
        # results = pd.DataFrame(results)
        return results
