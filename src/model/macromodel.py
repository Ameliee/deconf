# !/usr/bin/env python3

import json
import sys
from copy import deepcopy
from http.server import BaseHTTPRequestHandler, HTTPServer

from src.model.economie import Economie
from src.model.model_seair import SEAIRModel, SEAIR_ratios2params, measures2c


def mklist(nweeks):
    ans = [{"initial": []}]
    for _ in range(nweeks):
        ans.append({"week": []})
    return ans


def error_dumps(errstr):
    print('error:', errstr)
    return {'success': False, 'error': errstr}


def json2measure_days_c(eval_policy_req: dict, zone_idx: int, max_days: int):
    # compute measure days and estimate "c" according to the chosen data
    measure_days, myc = [], []
    for week_idx, week_nb in enumerate(eval_policy_req['measure_weeks']):
        measure_days.append(7 * week_nb)
        # The ... is not None part is to take in account that the field in data may not be available.
        tc = False if not eval_policy_req['forced_telecommuting'] else \
            eval_policy_req['forced_telecommuting'][week_idx]['activated'][zone_idx]
        sc = False if not eval_policy_req['school_closures'] else \
            eval_policy_req['school_closures'][week_idx]['activated'][zone_idx]
        so = False if not eval_policy_req['social_containment_all'] else \
            eval_policy_req['social_containment_all'][week_idx]['activated'][zone_idx]
        hr = False if not eval_policy_req['total_containment_high_risk'] else \
            eval_policy_req['total_containment_high_risk'][week_idx]['activated'][zone_idx]
        c = measures2c(forced_telecommuting=tc, school_closures=sc,
                       social_containment_all=so, total_containment_high_risk=hr)
        myc.append(c)
    # ensure we start at week 0
    if measure_days[0] != 0:
        measure_days.insert(0, 0)
        myc.insert(0, 0)  # no control measure
    # add measure ends and measure lifting (c=0)
    measure_days.append(max_days)
    myc.append(0)
    return measure_days, myc


class ModelRequestHandler(BaseHTTPRequestHandler):

    def eval_policy(self, eval_policy_req):
        # print('ModelRequestHandler::eval_policy()')
        # sanity checks
        if 'measure_weeks' not in eval_policy_req:
            return error_dumps('Missing weeks number for measures.')
        # check we use Alizon20
        if 'compartmental_model' not in eval_policy_req or eval_policy_req['compartmental_model'] != 'Alizon20':
            return error_dumps('Comparmental model not valid, use Alizon20.')
        if 'compartments' not in eval_policy_req \
                or eval_policy_req['compartments'] != ['S', 'E1', 'E2', 'A1', 'A2', 'I1', 'I2', 'R1', 'R2', 'M']:
            return error_dumps('Compartments not valid.')
        # check zones are valid and that population_size has the same length
        if 'zones' not in eval_policy_req or not eval_policy_req['zones']:
            return error_dumps('No zones provided.')
        nzones = len(eval_policy_req['zones'])
        if len(eval_policy_req['population_size']) != nzones:
            return error_dumps('Zone and population_size lengths do not match.')
        max_days = 1 + max(eval_policy_req['compartmental_model_parameters']['time_interval'])
        nweeks = int(max_days / 7)
        cases, hospital_beds, ICU_beds, new_cases, deaths = \
            mklist(nweeks), mklist(nweeks), mklist(nweeks), mklist(nweeks), mklist(nweeks)

        # now create and use a SEAIR model per zone
        for zone_idx, name in enumerate(eval_policy_req['zones']):
            npop = int(eval_policy_req['population_size'][zone_idx])
            # get initial ratios
            if name not in eval_policy_req['initial_ratios']:
                return error_dumps('No initial ratio for zone ' + name)
            zone_params = deepcopy(eval_policy_req['compartmental_model_parameters'])
            ratios = eval_policy_req['initial_ratios'][name]
            if len(ratios) != 10:
                return error_dumps('Incorrect initial ratio for zone ' + name)
            if not SEAIR_ratios2params(npop, *ratios, zone_params):
                return error_dumps('SEAIR_ratios2params() failed for zone ' + name)
            # create data and model
            measure_days, myc = json2measure_days_c(eval_policy_req=eval_policy_req, zone_idx=zone_idx, max_days=max_days)
            model = SEAIRModel(alizon20_parameters=zone_params)
            # use the SAIR model
            results = model.compute(c=myc, time_interval=measure_days, rtol=1e-3, atol=1e-3)
            # append to JSON dumps
            prev_cases = 0
            for week in range(nweeks + 1):  # include week 0
                S, E1, A1, I1, R1, E2, A2, I2, R2, M = results[week * 7][:]  # unpack week results
                key = 'initial' if week == 0 else 'week'
                all_cases = I1 + I2
                cases[week][key].append(all_cases)
                deaths[week][key].append(M)
                # "We assume that 30% of those that are hospitalised will require critical care" [Alizon20]
                # + "we assume that 50% of those in critical care will die" [Ferguson20]
                # I2 = "personnes infectées nécessitant une hospitalisation" [Alizon20]
                # theta = taux de létalité des infections hospitalisées = 0.15 [Alizon20]
                # --> ICU_beds = 30% * I2
                hospital_beds[week][key].append(I2)
                ICU_beds[week][key].append(.3 * I2)
                new_cases[week][key].append(0 if week == 0 else all_cases - prev_cases)
                prev_cases = all_cases

        # use economics model
        gdp_accumulated_loss = self.server.economy.evaluate(eval_policy_req, nweeks)
        # finally prepare JSON dumps
        dumps = {'success': True,
                 'zones': eval_policy_req['zones'],
                 'compartmental_model': eval_policy_req['compartmental_model'],
                 'max_days': max_days,
                 'gdp_accumulated_loss': gdp_accumulated_loss,
                 'cases': cases,
                 'hospital_beds': hospital_beds,
                 'ICU_beds': ICU_beds,
                 'deaths': deaths,
                 'new_cases': new_cases}
        return dumps

    def do_POST(self):
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        data = json.loads(post_body)
        self.send_response(200)
        self.end_headers()

        dumps = {'success': False, 'error': 'command not recognized'}
        if "eval_policy" in self.path:
            dumps = self.eval_policy(data)
        self.wfile.write(json.dumps(dumps).encode())


class ModelServer(HTTPServer):
    def __init__(self, server_address, RequestHandlerClass,
                 bind_and_activate=True, server_init=True):
        # stuff that needs to be stored here
        self.economy = Economie()
        if server_init:
            HTTPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate=bind_and_activate)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        host = 'localhost'
        port = 8000
    elif len(sys.argv) == 2:
        host = ':'.join(sys.argv[1].split(':')[:-1])
        port = int(sys.argv[1].split(':')[-1])
    else:
        print("usage : python macromodel.py hostname(default='localhost'):port(default=8000)")
        exit(1)

    server = ModelServer((host, port), ModelRequestHandler)
    print('Starting server at http://%s:%d' % (host, port))
    server.serve_forever()
